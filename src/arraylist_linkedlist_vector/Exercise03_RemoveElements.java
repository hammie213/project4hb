package arraylist_linkedlist_vector;

import java.util.LinkedList;
import java.util.List;

public class Exercise03_RemoveElements {
    public static void main(String[] args) {
        List<String> languages = new LinkedList<>();

        languages.add("Java");
        languages.add("JavaScript");
        languages.add("C#");
        languages.add("Python");
        languages.add("C++");

        /*
        Task
        Remove all elements that start with J and print the list


        System.out.println("FOR I LOOP");
        for(int i = 0; i < languages.size(); i++){
            if(languages.get(i).startsWith("J")){
                languages.remove(languages.get(i));
                i--;
            }
        }

         */
        System.out.println(languages);
        languages.removeIf(element -> element.startsWith("J"));
        System.out.println(languages);
        languages.removeIf(x -> x.length() > 5);
        System.out.println(languages);
    }
}
