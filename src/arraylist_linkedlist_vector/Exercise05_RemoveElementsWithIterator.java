package arraylist_linkedlist_vector;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static java.util.Arrays.asList;

public class Exercise05_RemoveElementsWithIterator {
    public static void main(String[] args) {
        ArrayList<String> objects = new ArrayList<>(Arrays.asList(
                "Pen",
                "Pencil",
                "Book",
                "Notebook",
                "MacBook Pro"
        ));

        /*
        TASK
        Remove elements that contains "book"
        -This is case-insensitive

        Print the list ->
         */

        Iterator<String> objectIterator = objects.iterator();

        while(objectIterator.hasNext()){
            String f = objectIterator.next();

            if(f.toLowerCase().contains("book")){
                objectIterator.remove();
            }
        }
        System.out.println(objects );
    }
}
