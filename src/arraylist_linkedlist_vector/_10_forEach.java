package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _10_forEach {
    public static void main(String[] args) {

    /*
    Create an arraylist to store below colors

    Blue
    Red
    Brown
    Pink
    Yellow
    Black

    Print the ArrayList
     */
        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Red", "Brown", "Pink", "Yellow", "Black"));
        System.out.println(colors);

        System.out.println("\n----------FOR I LOOP----------\n");
        for(int i = 0; i < colors.size(); i++){
            System.out.println(colors.get(i));
        }

        System.out.println("\n----------FOR EACH LOOP----------\n");
        colors.forEach(System.out::println);
    }
}
