package arraylist_linkedlist_vector;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class _06_Sorting {
    public static void main(String[] args) {
        LinkedList<Integer> numbers = new LinkedList<>();
        LinkedList<String> objects = new LinkedList<>();

        numbers.add(10);
        numbers.add(-3);
        numbers.add(5);
        numbers.add(15);

        objects.add("Remote");
        objects.add("Phone");
        objects.add("Laptop");

        System.out.println(numbers);
        System.out.println(objects);

        Collections.sort(numbers);
        Collections.sort(objects);

        System.out.println(numbers);
        System.out.println(objects);
    }
}
