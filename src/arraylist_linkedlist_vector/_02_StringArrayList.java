package arraylist_linkedlist_vector;

import java.util.ArrayList;

public class _02_StringArrayList {
    public static void main(String[] args) {
        /*
        Create an ArrayList and store below cities in it
        Chicago
        Berlin
        Miami

        Print size of the ArrayList
        Print the ArrayList

        Expected:
        3
        [Chicago, Berlin, Miami]
         */

        System.out.println("----------TASK ONE----------");

        ArrayList<String> cities = new ArrayList<>();

        cities.add(0, "Chicago");
        cities.add(1, "Berlin");
        cities.add(2, "Miami");

        System.out.println(cities.size());
        System.out.println(cities);

        System.out.println("\n----------TASK TWO----------\n");
        cities.set(2, "Evanston");
        System.out.println(cities);

        System.out.println("\n----------TASK THREE----------\n");
        cities.remove(1);
        System.out.println(cities);

        System.out.println("\n----------TASK FOUR----------\n");
        cities.set(0, "Evanston");
        cities.set(1, "New York");
        cities.add(2, "Rome");
        cities.add(3, "Gent");

        System.out.println(cities);

        System.out.println("\n----------TASK FIVE----------\n");

        cities.clear();
        System.out.println(cities);
        System.out.println(cities.size());

        System.out.println("\n----------TASK SIX----------\n");


    }
}
