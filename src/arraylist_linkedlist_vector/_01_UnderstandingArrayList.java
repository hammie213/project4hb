package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_UnderstandingArrayList {
    public static void main(String[] args) {

        //1. Declaring an array vs declaring an ArrayList

        String[] names1 = new String[3];

        ArrayList<String> names2 = new ArrayList<>(); // capacity = 10 by default

        //2. Getting the size of the array vs ArrayList
        System.out.println("\n----------Size of array vs ArrayList----------\n");
        System.out.println("The size of the array = " + names1.length);
        System.out.println("The size of the ArrayList = " + names2.size());

        //3. Printing an array vs ArrayList
        System.out.println("\n----------Printing array vs ArrayList----------\n");
        System.out.println("The array = " + Arrays.toString(names1));
        System.out.println("the ArrayList = " + names2);

        //4. Adding elements to specific indexes
        names1[0] = "Alex";
        names2.add(0, "John");


        /*
        Add Ali and Andrii to names1
        Add Joe and Jane to names2
        Print names1 and names2
         */
        names1[0] = "Ali";
        names1[1] = "Andrii";
        names2.add(1, "Joe");
        names2.add(2, "Jane");
        names2.add(3, "Abdallah");
        names2.add(4, "Vlad");
        names2.add(5, "Saeed");
        names2.add(6, "Suzanne");
        names2.add(7, "Hazal");
        names2.add(8, "Yildiz");
        names2.add(9, "Samir");

        System.out.println("\n----------Updated printing array vs ArrayList----------\n");
        System.out.println("The array = " + Arrays.toString(names1));
        System.out.println("The ArrayList = " + names2);
        System.out.println("The size of the ArrayList = " + names2.size());

        //5. Updating an existing element in an array vs ArrayList
        names1[1] = "Mike";
        names2.set(1, "Olena");
    }
}
