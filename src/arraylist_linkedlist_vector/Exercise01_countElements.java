package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise01_countElements {
    public static void main(String[] args) {
        /*
        Create an ArrayList and store below elements
        Blue
        Brown
        Pink
        Yellow
        Red
        Purple
         */

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Pink", "Yellow", "Red", "Purple"));

        System.out.println(colors);
        System.out.println(colors.size());
        for(String element : colors){
            System.out.println(element);
        }

        System.out.println("----------TASK 2----------");
        int counter = 0;
        for(String element : colors){
            if(element.length() == 6){
                counter++;
            }
        }
        System.out.println(counter);

        System.out.println("----------TASK 3----------");
        int counter2 = 0;
        for(String element : colors){
            if(element.toLowerCase().contains("o")) counter2++;
        }
        System.out.println(counter2);

        //2nd way
        int counter2a = 0;
        for(int i = 0; i < colors.size(); i++){
            if(colors.get(i).toLowerCase().contains("o")) counter2a++;
        }
        System.out.println(counter2a);


    }
}
