package casting;

public class Exercise6_CalculateSalary {
    public static void main(String[] args) {
        String salary1 = "5000";
        String salary2 = "6000.25";
        String salary3 = "4000.50";


        double s1 = Double.parseDouble(salary1);
        double s2 = Double.parseDouble(salary2);
        double s3 = Double.parseDouble(salary3);

        /*
        Task 1: Find min and max salary
         */

        System.out.println("The minimum salary is = " + Math.min(Math.min(s1,s2), s3));
        System.out.println("The maximum salary is = " + Math.max(Math.max(s1,s2), s3));

        /*
        Task 2: Calculate 10% of the min salary
        Expected output: 400.05
         */

        System.out.println("10 percent of the minimum salary is = " + Math.min(s3, Math.min(s1,s2)) * 0.1);
    }
}
