package casting;

public class Min_Max_Of_Primitives {
    public static void main(String[] args) {
        System.out.println(Byte.MIN_VALUE);
        System.out.println(Byte.MAX_VALUE);

        System.out.println("The min value of short = " + Short.MIN_VALUE);
        System.out.println("The min value of short = " + Short.MAX_VALUE);

    }
}
