package casting;

public class Exercise7_CalculateAbsoluteDifference {
    public static void main(String[] args) {
        String s1 = "100", s2 = "150", s3 = "50";
        /*
        Task:
        Find absolute difference between s1 and s2
        Find absolute difference between s3 and s2
        Find absolute difference between s3 and s1

        Expected output:
        50
        100
        50
         */

        int n1 = Integer.parseInt(s1);
        int n2 = Integer.parseInt(s2);
        int n3 = Integer.parseInt(s3);

        System.out.println(Math.abs(n1-n2));
        System.out.println(Math.abs(n3-n2));
        System.out.println(Math.abs(n3-n1));
    }
}
