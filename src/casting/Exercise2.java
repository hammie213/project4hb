package casting;

public class Exercise2 {
    public static void main(String[] args) {
        System.out.println('A' + 'b' + 20 + " Hello" + 2 + 1);
        // the 'A' + 'b' part is essentially adding the ASCII value numbers
        System.out.println('A' + 'b' + 20 + " Hello" + (2 + 1));
    }
}
