package casting;

public class CastingChars {
    public static void main(String[] args){
        int i1 = 65;
        System.out.println(i1); //65

        char c1 = (char)i1;  //65
        System.out.println(i1);

        //OR

        System.out.println(51); //51
        System.out.println((char) 51); //3

        System.out.println((char) 123); // {

        System.out.println((char) 32); // space

        System.out.println((char) 3500);

        System.out.println((char) 2800);




        char char1 = 'A';
        char char2 = 97; //'a'

        // primitive + String -> String
        // String + primitive -> String
        // p + p + S          -> number String
        // p + S + p          -> String
        // 'A' + "" + 'a'     -> Aa

        System.out.println(char1 + char2);  // 65+97 -> 162
        System.out.println("" + char1 + char2); //Aa
        System.out.println("" + (char1+char2)); //162
        System.out.println(char1 + char2 + ""); //162
        System.out.println(char1 + "" + char2); //Aa

    }
}
