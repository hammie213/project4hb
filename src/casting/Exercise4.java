package casting;

public class Exercise4 {
    public static void main(String[] args) {

        System.out.println(true && false); //false
        // compiler error System.out.println("true" && "false");

        System.out.println(String.valueOf(true) + String.valueOf(false));  //truefalse
        System.out.println("" + true + false); //truefalse

        System.out.println(String.valueOf('A') + 'a'); // Aa
        System.out.println("" + 10.5 + 5);
    }
}
