package casting;

public class ExplicitCasting {
    public static void main(String[] args){
        //Explicit casting - narrowing - downcasting

        int age = 25;

        byte b = (byte) age;

        System.out.println(b);

        //data loss if bigger primitive holds data that cant be stored in smaller one

        int num1 = 5000;

        byte num2 = (byte) num1;


    }
}
