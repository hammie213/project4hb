package practice;

public class ArticlePractice {
    public static void main(String[] args) {
        String school1 = "TechGlobal";
        String school2 = "Moraine Valley Community College";
        String myFirstName = "Hamza";
        String myLastName = "Batroukh";
        int myAge = 21;
        double myHeight = 5.5;
        String myHobby = "going to the gym";

        System.out.println("My first name is " + myFirstName + " and my last name is " + myLastName + ".");
        System.out.println("I'm " + myAge + "," + " and my height is " + myHeight + ".");
        System.out.println("My favorite hobby is " + myHobby + ".");
        System.out.println("I am going to 2 schools. The first school is " + school1 + " and the second school is " + school2 + ".");


    }
}
