package practice;

public class StringPractice {
    public static void main(String[] args) {
        String schoolName = "TechGlobal";
        String myName = "Hamza";
        int myAge = 21;
        double myHeight = 5.5;
        boolean isEligibleToDrive = true;


        System.out.println("My school name is " + schoolName + ". " + "My name is " + myName +". " + "My age is " + myAge + ". " + "My height is " + myHeight + ". " + "I am eligible to drive " + "\"" + isEligibleToDrive + "\"" + ". " );
        System.out.println();
        System.out.println("\tJava is a high-level-hard, class-based, object-oriented programming language that is \ndesigned to have as few implementation “dependencies” as possible. It is a general- \npurpose programming language intended to let programmers write once, run \nanywhere (WORA), meaning that compiled Java code can run on all platforms that \nsupport Java without the need to recompile.");

    }
}
