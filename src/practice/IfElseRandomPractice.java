package practice;

public class IfElseRandomPractice {
    public static void main(String[] args) {

        int random = (int)(Math.random() * (51) +0);
        System.out.println(random);

        if(random >= 10 && random <= 25){
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }

        // (expression) ? "true" : "false;
        //Ternary solution
        System.out.println((random >= 10 && random <=25) ? "true" : "false");

        //Print the boolean expression
        System.out.println((random >= 10 && random <= 25));


        //task 2
        int random2 = (int)(Math.random() * (100-1+1)+1);

        if(random2>=1 && random2<=25){
            System.out.println(random2 + " is in the 1st quarter");
            System.out.println(random2 + " is in the 1st half");
        }
        else if(random2>=26 && random2<=50){
                System.out.println(random2 + " is in the 2nd quarter");
                System.out.println(random2 + " is in the 1st half");
        }
        //else if ()

    }
}
