package practice;

public class StringMethodPractices {
    public static void main(String[] args) {
        /*
        Public static method "isCharExisting" which will take a String, a char, and an index.
        Return true if char is existing in that string at the given index.

        Example:
        "John", "o", 1 --> true
        "John", "o", 2 --> false
        "Peter", "o", 1 --> false
        "Peter", "r", 4 --> true
         */
        System.out.println(StringMethodPractices.isCharExisting("John", 'o', 1));
        System.out.println(StringMethodPractices.isCharExisting("John", 'o', 2));
        System.out.println(StringMethodPractices.isCharExisting("peter", 'o', 1));
        System.out.println(StringMethodPractices.isCharExisting("peter", 'r', 4));

        System.out.println(StringMethodPractices.isCharContained("John", 'o'));
    }

    public static boolean isCharExisting(String str, char c, int index){
        return str.charAt(index) == c;
    }
    /*
    Create a public static method named as "isCharContained" which will take a String, a char
    It will return true if that char is existing in that string

    example:
    "John", "o" --> true
    "Peter", "o" --> false
    "Peter", "r" --> false
     */

    public static boolean isCharContained(String str, char c){
        return str.contains(String.valueOf(c));
        // alternative: return str.contains(c+ "");
    }

}
