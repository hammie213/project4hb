package practice;

public class MethodPractices {
    public static void main(String[] args) {
        MethodPractices.printStringDesc("FBT");
        System.out.println("Task 2");
        MethodPractices.middleChar("Farty");
        System.out.println("TASK 3");
        MethodPractices.divideString("YYEESSIIRR");
        System.out.println("TASK 4");
        MethodPractices.printIsFirstLastTwoSame("hhhh");
        MethodPractices.printIsFirstLastTwoSame("a");
    }

    public static void printStringDesc(String str){
        if(str.length()>=1) {
            String str2 = str.toLowerCase();
            System.out.println("Length is = " + str.length());
            System.out.println("First char is = " + str.charAt(0));
            System.out.println("Last char is = " + str.charAt(str.length()-1));
            if(str2.contains("a")||str.contains("e")||str.contains("i")||str.contains("o")||str.contains("u")) System.out.println("This String has vowel");
            else System.out.println("This String doesn't have vowel");
        }
        else if(str.length()==0) System.out.println("Length is zero");
    }

    public static void middleChar(String str){
        if(str.length()<3) System.out.println("Length is less than 3");
        else if(str.length()>=3){
            if(str.length()%2==0){
                System.out.println(str.substring(str.length()/2-1, str.length()/2+1));
            }
            else System.out.println(str.charAt(str.length()/2));
        }
    }

    public static void divideString(String str){
        if(str.length()<4) System.out.println("INVALID INPUT");
        else if(str.length()>=4){
            System.out.println("First 2 characters are = " + str.substring(0,2));
            System.out.println("Last 2 characters are = " + str.substring(str.length()-2));
            System.out.println("The other characters are = " + str.substring(2,str.length()-2));
        }
    }

    public static void printIsFirstLastTwoSame(String str){
        if(str.length()<2) System.out.println("Length is less than 2");
        else if(str.length()>=2){
            if(str.substring(0,2).equals(str.substring(str.length()-2))) System.out.println("true");
            else System.out.println("false");
        }
        // can also be written as System.out.println(word.length() < 2 ? "Length is less than 2" : word.substring(0,2).equals(word.substring(word.length()-2))
    }
}
