package practice;

public class BooleanAndCharPractice {
    public static void main(String[] args) {
        boolean negative = false;
        boolean positive = true;

        System.out.println("Negative = " + "\"" + negative + "\"");
        System.out.println("Positive = " + "\"" + positive + "\"");

        char myFirstChar = '$';
        char mySecondChar = 'a';
        char myThirdChar = 'l';

        //  char myFirstLetter = 'H';
     //   char mySecondLetter = 'a';
        //char myThirdLetter = 'm';
        //char myFourthLetter = 'z';
        //char myFifthLetter = 'a';

        //System.out.println("My name is: " + "\"" + myFirstLetter + mySecondLetter + myThirdLetter + myFourthLetter + myFifthLetter + "\"");
        //System.out.print(myFirstLetter);
        //System.out.print(mySecondLetter);
        //System.out.print(myThirdLetter);
        //System.out.print(myFourthLetter);
        //System.out.print(myFifthLetter);

        char firstLetter = 'C';
        char secondLetter = 'A';
        char thirdLetter = 'T';

        System.out.print(firstLetter);
        System.out.print(secondLetter);
        System.out.print(thirdLetter);

    }
}
