package practice.arrayListPractices;

import utilities.RandomNumberGenerator;

import java.util.ArrayList;

public class Contains2Or3 {
    public static void main(String[] args) {
        System.out.println(contains2Or3());
        System.out.println(contains7Or13());
    }

    /*
    Create a public static method generating 5 numbers from 1-10 (both included)
    then store all of them inside arrayList and return true if it contains 2 or 3
     */

    public static boolean contains2Or3(){
        ArrayList<Integer> randoms = new ArrayList<>();

        for(int i = 0; i < 5; i++){
            randoms.add(RandomNumberGenerator.getARandomNumber(1, 10));
        }
        System.out.println(randoms);

        return randoms.contains(2) || randoms.contains(3);
    }

    /*
    create public static method which generates 7 numbers between -20 and 20 (both included)
    then store all of them inside arrayList and return true if it is containing 7 or 13
     */

    public static boolean contains7Or13(){
        ArrayList<Integer> numbers = new ArrayList<>();
        for(int i = 0; i < 7; i++){
            numbers.add(RandomNumberGenerator.getARandomNumber(-20, 20));
        }
        System.out.println(numbers);
        return numbers.contains(7) || numbers.contains(13);
    }
}
