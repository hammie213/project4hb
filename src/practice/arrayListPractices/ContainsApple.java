package practice.arrayListPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class ContainsApple {
    public static void main(String[] args) {
        ArrayList<String> fruits1 = new ArrayList<>(Arrays.asList("banana", "orange", "Apple"));
        System.out.println(containsApple(fruits1));
    }
    /*
    Create a public static method which will take an arrayList then it will check if it contains "apple"

    NOTE: ignore cases

     */

    public static boolean containsApple(ArrayList<String> fruits){
        for(String fruit : fruits){
            if(fruit.toLowerCase().equals("apple")) return true;
        }
        return false;
    }
}
