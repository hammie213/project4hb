package practice;

public class IfElsePractice {
    public static void main(String[] args) {

        int myInt = 45;

        /*
        If the number is between 0-25 print "first quarter"
        If the number is between 25-49 print "second quarter"
        If the number is between 50-74 print "third quarter"
         */

        if(myInt >= 0 && myInt <= 24){
            System.out.println("first quarter");
        }
        else if(myInt >= 25 && myInt <= 49){
            System.out.println("second quarter");
        }
        else if(myInt >= 50 && myInt <= 74){
            System.out.println("third quarter");
        }

        /*
        If number between -50 and -1 print "number is in negative part"
        If number is 0 print "number is zero"
        If number between 1 and 50 print
         */

        if(myInt >= -50 && myInt <= -1){
            System.out.println("number is in negative part");
        }
        else if(myInt == 0){
            System.out.println("number is zero");
        }
        else if(myInt >= 1 && myInt <= 50){
            System.out.println("number is in positive part");
        }
    }
}
