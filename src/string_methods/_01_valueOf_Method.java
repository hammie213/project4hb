package string_methods;

public class _01_valueOf_Method {
    public static void main(String[] args) {
        int num = 125;

        String numString = String.valueOf(num);
//This is a comparison of the two
        System.out.println(num + 5); // 130
        System.out.println(numString + 5); // 1255
    }
}
