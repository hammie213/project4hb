package string_methods;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class Exercise02_StringComparison {
    public static void main(String[] args) {
        String str1 = "JAVA";
        String str2 = "java";

        System.out.println(str1.equalsIgnoreCase(str2));
        System.out.println(str1.toLowerCase().equals(str2));
        System.out.println(str1.equals(str2.toUpperCase()));
    }
    /*
    Assume that you are given below Strings

    JAVA
    java

    Compare these 2 strings and always get true;
     */


}
