package string_methods;

public class _04_equalsIgnoreCase_Method {
    public static void main(String[] args) {
        /*
        1. returns a boolean
        2. non-static
         */

        System.out.println("hello".equals("Hello")); // false
        System.out.println("hello".equalsIgnoreCase("Hello")); // true
    }
}
