package string_methods;

public class _12_contains_Method {
    public static void main(String[] args) {
        String s = "TechGlobal School";

        System.out.println(s.contains("Tech"));
        System.out.println(s.contains("School"));
        System.out.println(s.contains("a"));
        System.out.println(s.contains("E"));

        //IMPORTANT
        System.out.println(s.contains("")); // true
        System.out.println(s.contains(s)); // true

    }
    /*

     */


}
