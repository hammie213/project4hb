package string_methods;

public class _10_substring_Method {
    public static void main(String[] args) {
        /*
        returns a String
        non static
        It is overloaded and takes either one or 2 int index arguments
         */
        String s = "TechGlobal";

        //get "Global"
        String s1 = s.substring(4); // from index 4 to the end
        System.out.println(s1);

        //get "Tech"

        System.out.println(s.substring(0,4));
    }
}
