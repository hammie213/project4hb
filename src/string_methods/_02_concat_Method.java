package string_methods;

public class _02_concat_Method {
    public static void main(String[] args) {
        /*
        1. return - returns a String
        2. non-static
        3. takes one argument as String
        4.
         */

        String str1 = "Tech";
        String str2 = "Global";

        String str3 = str1.concat(str2);

        System.out.println(str3);
    }
}
