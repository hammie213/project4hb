package string_methods;

import utilities.ScannerHelper;

public class Exercise06_MiddleCharOrChars {
    public static void main(String[] args) {
        /*
        Olena   -> e -> 2 -> s.charAt(s.length()/2) this is for the middle character of odd strings
        Ali     -> l -> 1 -> s.charAt(s.length()/2)
        Suzanne -> a -> 3 -> s.charAt(s.length()/2)

        John     -> oh -> s.substring(1,3)
        Abdallah -> al -> s.substring(3,5)
        Yildiz   -> ld -> s.substring(2,4)
         */

        String name = ScannerHelper.getAName();

        if(name.length() % 2 == 0){
            //even
            System.out.println("Middle chars are = " + name.substring(name.length()/2-1, name.length()/2+1));
        }
        else{
            //odd
            System.out.println("Middle char is = " + name.charAt(name.length()/2));
        }
    }
}
