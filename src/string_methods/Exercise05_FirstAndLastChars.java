package string_methods;

import utilities.ScannerHelper;


public class Exercise05_FirstAndLastChars {
    public static void main(String[] args) {
        /*
        Ask user to enter name
        Print first char of name with message "First character in the name is = "
        Print last char of name with message "Last character in the name is = "
         */

        String name = ScannerHelper.getAName();
        System.out.println("First character in the name is = " + name.charAt(0));
        System.out.println("Last character in the name is = " + name.charAt(name.length()-1));



    }
}
