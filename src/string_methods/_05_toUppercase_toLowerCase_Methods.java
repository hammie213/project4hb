package string_methods;

public class _05_toUppercase_toLowerCase_Methods {
    public static void main(String[] args) {
        /*
        1. returns a string
        2. non static
        3. the one we use does not take any arguments - there are overloaded methods
        */
        String name = "John";

        System.out.println(name.toLowerCase()); // john
        System.out.println(name.toUpperCase()); // JOHN

        System.out.println("hello".toUpperCase());
        System.out.println("heLLO".toLowerCase());

        System.out.println("abc".toUpperCase().toLowerCase());

        System.out.println("ab".toUpperCase().concat("xY".toLowerCase())); //ABxy
        System.out.println("ab".toUpperCase().concat("xY".toLowerCase()).toLowerCase()); //abxy
    }
}
