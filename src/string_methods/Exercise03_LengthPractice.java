package string_methods;

import utilities.ScannerHelper;

public class Exercise03_LengthPractice {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter 2 strings
        store answers of user in different strings
        finally, print length of those strings with proper messages

        assume user entered "Java" and "C#"

        Expected output:
        The length of the first string = 4
        The length of the second string = 2
         */

        String str1 = ScannerHelper.getAString();
        String str2 = ScannerHelper.getAString();

        System.out.println(str1.length());
        System.out.println(str2.length());
    }
}
