package string_methods;

import java.util.Arrays;

public class _15_toCharArray_Method {
    public static void main(String[] args) {
        /*
        -returns a char array
        - non static
        - it doesnt take any args
         */

        //Turn string into an array

        String s = "TechGlobal School";

        System.out.println(Arrays.toString(s.toCharArray()));
    }
}
