package string_methods;

public class Exercise04_Substring {
    public static void main(String[] args) {
        /*
        assume given below sentence
        "The best is Java"
        Write a Java program that extracts "Java" from given sentence
        and store extracted value in another String
        Finally print the extracted String
         */

        String s1 = "The best is Java";
        String s2 = s1.substring(12);
        System.out.println(s2);

        /*
        Task 2:
        Assume given
        "Java is an object-oriented programming language"
        Write program to extract "Java" and "object-oriented" from given sentence and store in seperate strings
        print extracted strings
         */

        String str1 = "Java is an object-oriented programming language";
        String str2 = str1.substring(0,4);
        String str3 = str1.substring(11,26);

        System.out.println(str2);
        System.out.println(str3);
    }
}
