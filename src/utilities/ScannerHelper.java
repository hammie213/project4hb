package utilities;

import java.util.Scanner;

public class ScannerHelper {
    /*
    Create a method that asks user to enter a name and RETURN it
     */

    static Scanner input = new Scanner(System.in);
    public static String getAName(){
        System.out.println("Please enter a name:");
        String name = input.nextLine();

        return name;

    }
    /*
    Create method that asks user to enter an age and RETURN it
     */
    public static int getAnAge(){
        System.out.println("Please enter an age:");
        int age = input.nextInt();

        return age;
    }

    public static String getAnAddress(){
        System.out.println("Please enter an address: ");
        String address = input.nextLine();

        return address;
    }

    public static String getAString(){
        System.out.println("Please enter a String");
        return input.nextLine();
    }

    public static String favColors(){
        System.out.println("Please enter your favorite color:");
        String color = input.nextLine();
        return color;
    }
    public static int getANumber(){
        System.out.println("Please enter a number:");
        int num = input.nextInt();
        input.nextLine();

        return num;
    }

}
