package utilities;

public class MathHelper {
    /*
    Math.max(Math.max(num1, num2), num3)
     */

    public static int maxOfThree(int num1, int num2, int num3){
        return Math.max(Math.max(num1, num2), num3);
    }

    /*
    Write a method that returns the min number of 3 int numbers
     */

    public static int minOfThree(int num1, int num2, int num3){
        return Math.min(Math.min(num1, num2), num3);
    }

    /*
    Write a method that returns the middle number of 3 int numbers
    Assume numbers can never be equal to each other
     */

    public static int middleOfThree(int num1, int num2, int num3){
        int max = maxOfThree(num1, num2, num3);
        int min = minOfThree(num1, num2, num3);

        return num1+num2+num3 - min - max;
    }
    /*
    Create a method that takes an int as an argument and returns true if it is even returns false if it is odd
     */

    public static boolean evenOrFalse(int numberOne){

        //ternary method: boolean even = numberOne % 2 == 0 ? true : false;
        //ternary method: return even;
        //simplest method: return numberOne % 2 == 0;
        if(numberOne % 2 ==0) return true;
        else return false;
    }

    public static boolean isOdd(int numberTwo){
        if(numberTwo % 2 == 0) return false;
        return true;
    }

    /*
    Create a method that takes 2 int arguments and returns their sums
     */

    public static int sum(int num1, int num2){
        return num1 + num2;
    }

    //OVERLOADING A METHOD
    public static int sum(int num1, int num2, int num3){
        return num1 + num2 + num3;
    }
    public static double sum(double num1, double num2){
        return num1 + num2;
    }
    public static long sum(long num1, long num2){
        return num1 + num2;
    }
    public static int sum(byte num1, int num2){
        return num1 + num2;
    }
}
