package characterClass;

public class EasyMethodPractices {
    public static void main(String[] args) {
        String s1 = "2860 south river road des plaines il";
        System.out.println("String: " + s1 + " has " + countLetters(s1) + " characters");
    }


    public static int countLetters(String s){
        int counter = 0;
        for(int i = 0; i < s.length(); i++){
            if(Character.isLetter(s.charAt(i))){
                counter++;
            }
        }
        return counter;
    }
}
