package collections;

import java.util.*;

public class ___MyPractice___ {
    public static void main(String[] args) {


        LinkedList<String> linkedList = new LinkedList<>();

        linkedList.add("iPhone");
        linkedList.add("iPad");
        linkedList.add("iPod");
        linkedList.add("MacBook Air");
        linkedList.add("MacBook Pro");
        linkedList.add("Apple Watch");
        linkedList.add("iMac");
        linkedList.add("AirTag");
        linkedList.add("Apple TV");

        System.out.println("\n------Task Two------\n");
        for(String element : linkedList){
            System.out.println(element);
        }

        System.out.println("\n------Task Three------\n");
        Iterator iterator = linkedList.listIterator(1);

        while (iterator.hasNext()) System.out.println(iterator.next());

        System.out.println("\n------Task Four------\n");

        Iterator descendingIterator = linkedList.descendingIterator();

        while (descendingIterator.hasNext()) System.out.println(descendingIterator.next());

        System.out.println("\n------Task Five------\n");

        linkedList.add(1, "Airpods");
        System.out.println(linkedList);

        System.out.println("\n------Task Six------\n");


    }
}



