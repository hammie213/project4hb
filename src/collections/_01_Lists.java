package collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

public class _01_Lists {
    public static void main(String[] args) {
        /*
        -They preserve insertion order
        -They allow duplicates
        -They allow null elements

        -Performance
        -Memory
        -Initial capacity
        -Load
        -The way they store the data
         */
        LinkedList<String> fruits = new LinkedList<>();

        fruits.add("Apple"); // adds to list
        fruits.offer("Orange"); // adds to list
        fruits.offerFirst("Pineapple"); // becomes the first element

        System.out.println(fruits.element()); // basically like getFirst but comes from Queue
        System.out.println(fruits.getFirst()); // comes from DeQueue

        System.out.println(fruits);

        fruits.clear();

        System.out.println(fruits.poll());
        System.out.println(fruits.remove());

    }
}