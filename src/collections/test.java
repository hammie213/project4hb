package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeSet;

public class test {
    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(5);
        arr.add(2);
        arr.add(1);
        arr.add(8);

        System.out.println(findMin(arr));

        int[] arr2 = {5, 7, 15, 10, 20};
        System.out.println(Arrays.toString(subtract5(arr2)));
    }

    public static int findMin(ArrayList<Integer> myList){
        return new TreeSet<>(myList).first();
    }

    /*
    Write a public static method that takes an int array as an argument and returns an int array.
-The method should subtract 5 from each element and return it back.
-Method name can be called as subtract5.
     */

    public static int[] subtract5(int[] arr){

        for(int i = 0; i < arr.length; i++){
            arr[i] = arr[i] - 5;
        }
        return arr;
    }
}
