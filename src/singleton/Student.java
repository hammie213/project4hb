package singleton;

public class Student {

    public static Student student;

    private Student(){}

    public static Student getStudent(){
        if(student == null) student = new Student();
        return student;
    }

}

// Singleton design pattern
//1. Make constructor private
//2. Create an instance variables as same class data type
//3. Provide a getter method to get single object
