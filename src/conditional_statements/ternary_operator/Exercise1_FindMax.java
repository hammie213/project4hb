package conditional_statements.ternary_operator;

public class Exercise1_FindMax {
    public static void main(String[] args) {
        /*
        TASK
        Write a program that generates 2 random numbers between 1 and 25
        Find and print the greatest number
         */

        int random1 = (int)(Math.random() * (25-1+1) +1);
        int random2 = (int)(Math.random() * (25-1+1) +1);

        System.out.println("The random number 1 = " + random1);
        System.out.println("The random number 2 = " + random2);


        System.out.println("------SOLUTION WITH MATH CLASS------");
        int max1 = Math.max(random1, random2);

        System.out.println(max1);

        System.out.println("------SOLUTION WITH IF ELSE------");
        int max2 = Integer.MIN_VALUE;

        if(random1 > random2) max2 = random1;
        else max2 = random2;

        System.out.println(max2);


        System.out.println("------Solution with ternary------");

        int max3 = random1 > random2 ? random1 : random2;
        System.out.println(max3);

    }
}
