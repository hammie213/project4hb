package conditional_statements.ternary_operator;

import java.util.Random;

public class Exercise2_FindMin {
    public static void main(String[] args) {

        /*
    TASK
    Write a program that generates 2 random numbers between 1 and 10
    Find and print the smallest number
    USE TERNARY
     */


        Random random = new Random();
        int num1 = random.nextInt(10) + 1;  //0 to 9
        int num2 = random.nextInt(10) + 1;  //0 to 9
        System.out.println("Random 1 = " + num1);
        System.out.println("Random 1 = " + num2);

        /*
        int random1 = (int)(Math.random() * (10)+1);
        int random2 = (int)(Math.random() * (10)+1);

        int min1 = random1 > random2 ? random2 : random1;
        System.out.println(min1);
        */


    }




}
