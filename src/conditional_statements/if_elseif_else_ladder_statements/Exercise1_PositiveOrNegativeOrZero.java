package conditional_statements.if_elseif_else_ladder_statements;

import java.util.Scanner;

public class Exercise1_PositiveOrNegativeOrZero {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a number
        If number is more than 0, print "POSITIVE"
        If number is less than 0, print "NEGATIVE"
        Otherwise print "ZERO"
         */

        Scanner input = new Scanner(System.in);

        System.out.println("Hey user, enter a number: ");
        int number = input.nextInt();

        if(number > 0){
            System.out.println("POSITIVE");
        }
        else if(number == 0){
            System.out.println("ZERO");
        }
        else{
            System.out.println("NEGATIVE");
        }
    }
}
