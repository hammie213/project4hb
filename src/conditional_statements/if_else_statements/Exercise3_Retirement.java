package conditional_statements.if_else_statements;

import java.util.Scanner;

public class Exercise3_Retirement {
    public static void main(String[] args) {

        Scanner age = new Scanner(System.in);

        System.out.println("Enter your age: ");
        int ageEntered = age.nextInt();

        if(ageEntered >= 55){
            System.out.println("It is your time to get retired");
        }
        else{
            System.out.println("You can retire in: " + Math.abs(55 - ageEntered) + " years.");
        }

    }
}
