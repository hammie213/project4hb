package conditional_statements.if_else_statements;

public class IfElseSyntax {
    public static void main(String[] args) {
        /*
        - if or else statements are used to control the flow of the program based on conditions
        - conditions are always booleans statements (true or false)
         */

        boolean condition = false;

        if(condition){
            System.out.println("AAA");
        }
        else{
            System.out.println("BBB");
        }

        System.out.println("End of the program!");
    }
}
