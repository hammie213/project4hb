package recursion;

public class UnderstandingRecursion {

    public static void print(){
        System.out.println("Hello");
        print();
    }

    // create a method that prints numbers
    public static void printNumbers(int end){
        for(int i = 1; i <= end; i++){
            System.out.println(i);
        }
    }

    //Recursive way
    public static void printNumbersRecursively(int end){ // 5
        if(end == 1) return;
        printNumbersRecursively(end - 1 );
        System.out.println(end);
    }

    public static void main(String[] args) {
        print();
    }
}
