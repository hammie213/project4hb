package scanner_programs;

import com.sun.deploy.security.BadCertificateDialog;

import java.util.Scanner;

public class FindSum {
    public static void main(String[] args) {
        int number1, number2, number3;

        Scanner inputReader = new Scanner(System.in);

        System.out.println("Please enter number 1: ");
        number1 = inputReader.nextInt();

        System.out.println("Please enter number 2: ");
        number2 = inputReader.nextInt();

        System.out.println("Please enter number 3: ");
        number3 = inputReader.nextInt();

        int sum = number1 + number2 + number3;

        System.out.println("The sum of the numbers you entered is " + sum);




    }
}
