package scanner_programs;

import java.util.Scanner;

public class FirstScannerProgram {
    public static void main(String[] args) {

        //1. Create a scanner object

        //dataType varName = value;

        String firstName, lastName;

        Scanner inputReader = new Scanner(System.in);

        System.out.println("Please enter your first name:");
        firstName = inputReader.nextLine(); // The first name you enter

        System.out.println("Please enter your last name:");
        lastName = inputReader.next();

        System.out.println("Your full name is = " + firstName + " " + lastName);
    }
}
