package date_time;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDates {
    public static void main(String[] args) {

        System.out.println("\n------Formatting Date------\n");
        Date date = new Date();
        System.out.println(date);

        //print the date in the format of MM/dd/yyyy 12/11/2022
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        System.out.println(formatter.format(date));

        //Print the date in the format of yyyy 2022
        formatter = new SimpleDateFormat("yyyy");
        System.out.println(formatter.format(date));

        System.out.println(new SimpleDateFormat("hh 'o''clock' a, zzzz").format(new Date()));
    }
}
