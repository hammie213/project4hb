package date_time;

import utilities.ScannerHelper;

import java.time.LocalDate;
import java.util.Date;

public class CalculateAge {
    public static void main(String[] args) {
        /*
        Ask user to enter their age and find their year of birth
         */

        int age = ScannerHelper.getAnAge();

        LocalDate localDate = LocalDate.now();

        int year = localDate.getYear();

        System.out.println(year-age);

    }
}
