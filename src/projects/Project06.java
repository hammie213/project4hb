package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Project06 {
    public static void main(String[] args) {
        String[] array = {"foo", "", " ", "foo bar", "java is fun", " ruby "};
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15));
        String password = "abcd1234!A";
        String email = "h@gmail.com";
        System.out.println(Arrays.toString(array));
        System.out.println(countMultipleWords(array));
        System.out.println(removeNegatives(numbers));
        System.out.println(validatePassword(password));
        System.out.println(validateEmailAddress(email));

    }

    public static int countMultipleWords(String[] array) {
        int counter = 0;
        for (String words : array) {
            for (int i = 1; i < words.length()-1; i++) {
                if (words.charAt(i) == ' ' && Character.isLetter(words.charAt(i - 1)) && Character.isLetter(words.charAt(i + 1))) {
                    counter++;
                    break;
                }
            }
        }
        return counter;
    }

    public static ArrayList removeNegatives(ArrayList<Integer> numbers){
        numbers.removeIf(x -> x < 0);
        return numbers;
    }

    public static boolean validatePassword(String password){
        return Pattern.matches("(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9](?=.*[!])).{8,16}", password);
    }

    public static boolean validateEmailAddress(String email){
        return Pattern.matches("^[A-Za-z0-9+_.-]+@(.+)$", email);
    }
}
