package projects;

import utilities.ScannerHelper;


import java.util.Scanner;

public class Project04 {
    public static void main(String[] args) {
        System.out.println("----------TASK ONE----------");
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a word");
        String wordT1 = input.next();


        if(wordT1.length() < 8) System.out.println("This String does not have 8 characters");
        else{
            System.out.println(wordT1.substring(wordT1.length()-4) + wordT1.substring(4,wordT1.length()-4)+ wordT1.substring(0,4));
        }



        System.out.println();
        System.out.println("----------TASK TWO----------");

        System.out.println("Please enter a sentence");
        String sentenceT2 = input.nextLine();
        int spaceCounter = 0;
        int beginningOfLastWord = 0;
        int endOfFirstWord = 0;

        for(int i = 0; i < sentenceT2.length(); i++){
            if(sentenceT2.charAt(i) == ' ') spaceCounter++;
        }
        if(spaceCounter < 1) System.out.println("This sentence does not have 2 or more words to swap");
        else{
            for(int i = sentenceT2.length()-1; i>= 0; i--){
                if(sentenceT2.charAt(i) == ' '){
                    beginningOfLastWord += i;
                    break;
                }
            }
            for(int i = 0; i <= sentenceT2.length()-1; i++){
                if(sentenceT2.charAt(i) == ' '){
                    endOfFirstWord += i;
                    break;
                }
            }
        }

        System.out.println(sentenceT2.substring(beginningOfLastWord+1) + sentenceT2.substring(endOfFirstWord, beginningOfLastWord) + " " + sentenceT2.substring(0, endOfFirstWord));


        System.out.println();
        System.out.println("----------TASK THREE----------");

        String str1Task3 = "These books are so stupid";
        String str2Task3 = "I like idiot behaviors";
        String str3Task3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        for(int i = 0; i < str1Task3.length(); i++){
            if(str1Task3.contains("stupid") || str1Task3.contains("idiot")){
                str1Task3 = str1Task3.replace("stupid", "nice");
                str1Task3 = str1Task3.replace("idiot", "nice");
            }
        }
        for(int i = 0; i < str2Task3.length(); i++){
            if(str2Task3.contains("stupid") || str2Task3.contains("idiot")){
                str2Task3 = str2Task3.replace("stupid", "nice");
                str2Task3 = str2Task3.replace("idiot", "nice");
            }
        }
        for(int i = 0; i < str3Task3.length(); i++){
            if(str3Task3.contains("stupid") || str3Task3.contains("idiot")){
                str3Task3 = str3Task3.replace("stupid", "nice");
                str3Task3 = str3Task3.replace("idiot", "nice");
            }
        }

        System.out.println(str1Task3);
        System.out.println(str2Task3);
        System.out.println(str3Task3);



        System.out.println();
        System.out.println("----------TASK FOUR----------");

        System.out.println("Please enter your name");
        String nameTask4 = input.nextLine();

        if (nameTask4.length() < 2) System.out.println("Invalid input!!!");
        else if(nameTask4.length() > 2 && nameTask4.length() % 2 == 0){
            System.out.println(nameTask4.substring(nameTask4.length()/2-1, nameTask4.length()/2+1));
        }
        else System.out.println(nameTask4.charAt(nameTask4.length()/2));



        System.out.println();
        System.out.println("----------TASK FIVE----------");

        System.out.println("Please enter a country");
        String country = input.next();

        if(country.length() < 5) System.out.println("Invalid input!!!");
        else System.out.println(country.substring(2, country.length()-2));



        System.out.println();
        System.out.println("----------TASK SIX----------");

        String address = ScannerHelper.getAnAddress();

        address = address.replace('A', '*');
        address = address.replace('a', '*');
        address = address.replace('E', '#');
        address = address.replace('e', '#');
        address = address.replace('I', '+');
        address = address.replace('i', '+');
        address = address.replace('O', '@');
        address = address.replace('o', '@');
        address = address.replace('U', '$');
        address = address.replace('u', '$');

        System.out.println(address);



        System.out.println();
        System.out.println("----------TASK SEVEN----------");


        int random1 = (int) (Math.random() * 26);
        int random2 = (int) (Math.random() * 26);
        System.out.println("Random 1: " + random1);
        System.out.println("Random 2: " + random2);

        if(random1 > random2){
            for(int i = random2; i <= random1; i++){
                if(i % 5 != 0){
                    System.out.println(i);
                }
                else continue;
            }
        }
        else{
            for(int i = random1; i <= random2; i++){
                if(i % 5 != 0){
                    System.out.println(i);
                }
                else continue;
            }
        }


        System.out.println();
        System.out.println("----------TASK EIGHT----------");

        int wordCounterTask8 = 0;

        System.out.println("Please enter a sentence: ");
        String sentenceTask8 = input.nextLine();

        for(int i = 0; i < sentenceTask8.length(); i++){
            if(sentenceTask8.charAt(i) == ' ') wordCounterTask8++;
        }

        wordCounterTask8 += 1;

        if(wordCounterTask8 < 1) System.out.println("This sentence does not have multiple words");
        else System.out.println("This sentence has " + wordCounterTask8 + " words.");



        System.out.println();
        System.out.println("----------TASK NINE----------");

        int numTask9 = ScannerHelper.getANumber();

        for(int i = 1; i <= numTask9; i++){
            if(i % 2 == 0){
                if(i % 3 == 0){
                    System.out.println("FooBar");
                }
                else System.out.println("Foo");
            }
            else if(i % 3 == 0){
                System.out.println("Bar");
            }
            else System.out.println(i);

        }

        System.out.println("----------TASK TEN----------");

        String palindromeReversed = "";
        System.out.println("Please enter a word: ");
        String palindrome = input.next();

        if(palindrome.length() < 1) System.out.println("This word does not have 1 or more characters");
        for(int i = palindrome.length()-1; i >= 0; i--){
            // add each character to an empty string backwards
            palindromeReversed += palindrome.charAt(i);
        }
        if(palindrome.equals(palindromeReversed)) System.out.println("This word is palindrome");
        else System.out.println("This word is not palindrome");

        System.out.println("----------TASK ELEVEN----------");

        System.out.println("Please enter a sentence: ");
        String sentenceTask11 = input.nextLine();
        int aCounter = 0;

        if(sentenceTask11.length() < 1) System.out.println("This sentence does not have any characters");

        for(int i = 0; i < sentenceTask11.length(); i++){
            if(sentenceTask11.charAt(i) == 'a' || sentenceTask11.charAt(i) == 'A') aCounter++;
        }
        System.out.println("This sentence has " + aCounter + " a or A letters.");
    }
}
