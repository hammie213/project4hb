package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("\n------Task One------\n");
        int[] array = {4, 8, 7, 15};
        System.out.println(findClosestDistance(array));

        System.out.println("\n------TASK TWO------\n");
        int[] array2 = {5, 3, -1, 3, 5, 7, -1};
        System.out.println(findSingleNumber(array2));

        System.out.println("\n------TASK THREE------\n");
        String str = "Hello";
        System.out.println(findFirstUniqueCharacter(str));

        System.out.println("\n------TASK FOUR------");
        int[] array4 = {4, 7, 8, 6};
        System.out.println(findMissingNumber(array4));
    }

    public static int findClosestDistance(int[] array){
        if(array.length < 2) return -1;
        int difference = Integer.MAX_VALUE;
        for(int i = 0; i < array.length-1; i++){
            for(int j = i+1; j < array.length; j++) {
                if (Math.abs(array[i] - array[j]) < difference) difference = Math.abs(array[i] - array[j]);
            }
        }
        return difference;
    }

    public static int findSingleNumber(int[] array){
        Arrays.sort(array);
        for(int i = 0; i < array.length-1; i++){
            if(array[i] != array[i+1]) return array[i];
            else{
                i++;
                continue;
            }
        }
        return -111;
    }

    public static char findFirstUniqueCharacter(String str){
        char isUnique = ' ';
        for(int i = 0; i < str.length(); i++){
            for(int j = i+1; j < str.length(); j++){
                if(str.charAt(i) != str.charAt(j)) isUnique = str.charAt(i);
                else continue;
            }
        }
        return isUnique;
    }

    public static int findMissingNumber(int[] array){
        Arrays.sort(array);
        for(int i = 0; i < array.length-1; i++){
            if(array[i+1] == array[i] + 1) continue;
            else return array[i] + 1;
        }
        return -11;
    }


}
