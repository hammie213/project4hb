package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {

        //variable declaration and assignment with initial value
        int age = 45;

        System.out.println(age);

        //variable declaration and allocates the memory
        String name;

        //variable assignment
        name = "John";

        System.out.println(name); //john

        //Re-assigning
        name = "Mike";

        System.out.println(name); //mike

        age = 21;

        System.out.println(age); //21



    }
}
