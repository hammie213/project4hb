package primitives;

import java.sql.SQLOutput;

public class Numbers {
    public static void main(String[] args) {
        /*
        John
        his age -> 45
        his balance -> 400.45
         */

        //dataType variableName = value;
        long age = 45;
        long favNumber = 70;

        double balance1 = 23.2323;
        double balance2 = 31.42342;

        double price = 24.5;

        char favChar = 'H';
        char dollarSign = '$';



        System.out.println(age);
        System.out.println(favChar);
        System.out.println(balance1);
        System.out.println(price);
        System.out.println(favNumber);
    }
}
