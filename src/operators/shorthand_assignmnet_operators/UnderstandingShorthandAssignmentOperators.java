package operators.shorthand_assignmnet_operators;

public class UnderstandingShorthandAssignmentOperators {
    public static void main(String[] args) {
        int age = 10;

        System.out.println(age);

        //What will the age be 5 years later

        age += 5;
        System.out.println(age);

        //task 2

        int num1 = 50;

        num1 = num1 % 12;

        //or

       // num1 %= 12; they both mean the same thing

        System.out.println(num1);
    }
}
