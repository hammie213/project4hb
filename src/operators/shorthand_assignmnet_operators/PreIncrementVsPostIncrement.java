package operators.shorthand_assignmnet_operators;

public class PreIncrementVsPostIncrement {
    public static void main(String[] args) {
        //Post increment
        int num1 = 5;

        num1++;   // this means increase it by one but assign it later  | num1 = 5
        System.out.println(num1);

        //Pre increment
        int num2 = 5;

        ++num2;   // this means increase it by one and assign it now   | num2 = 6
        System.out.println(num2);

        byte b1 = 5, b2 = 5;

        System.out.println(b1++);
        System.out.println(++b2);
    }
}
