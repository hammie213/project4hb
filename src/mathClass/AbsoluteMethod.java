package mathClass;

import java.util.Scanner;

public class AbsoluteMethod {

   public static void main(String[] args) {

       Scanner input = new Scanner(System.in);

       System.out.println("Difference between your numbers is = " + Math.abs(10-14));
       System.out.println("Difference between your numbers is = " + Math.abs(14-10));
       System.out.println("Difference between your numbers is = " + (10-14));


       //TASK

       /*
       Write a program asking user two ages and gets the difference between ages
       and prints them

       Output:
       "Please enter an age"
       input: 24
       "Please enter another age"
       input: 32

       "Difference between your ages is = 8"
        */

       int age, age2;
       System.out.println("Please enter an age");
       age = input.nextInt();
       System.out.println("Please enter another age");
       age2 = input.nextInt();

       System.out.println("Difference between your ages is = " + Math.abs(age-age2));
       System.out.println("Biggest age is = " + Math.max(age, age2));
       System.out.println("Smallest age is = " + Math.min(age, age2));

   }
}
