package mathClass;

public class MinAndMaxMethods {
    public static void main(String[] args) {

        int maxNumber = Math.max(80, 27);  //80

        int maxNumber2 = Math.max(maxNumber, 125);

        int maxNumber3 = Math.max(maxNumber2, 625);

        System.out.println("Max number = " + maxNumber);
        System.out.println("Max number 2 = " + maxNumber2);
        System.out.println("Max number 3 = " + maxNumber3);

        int minNumber = Math.min(80, 27);

        int minNumber2 = Math.min(Math.min(80, 27), 2);

        int minOfTwoNeg = Math.min(-40, -60);
        System.out.println("Min number = " + minNumber);
        System.out.println("Min number 2 = " + minNumber2);
        System.out.println("Min of two neg = " + minOfTwoNeg);

        double maxOfTwoDecimals = Math.max(1.7, 6.5);
        double maxOfTwoDecimalsNeg = Math.max(-5.4, -27.2);
        System.out.println("maxOfTwoDecimals = " + maxOfTwoDecimals);


        int max1 = Math.max(6, 9);
        System.out.println("max1 = " + max1);

        int max2 = Math.max(17, 49);
        System.out.println("max2 = " + max2);

        System.out.println("double1 = " + Math.max(34.2, 12.5));

        System.out.println("max 3 = " + Math.max(-14, -32));

        System.out.println("max 4 = " + Math.max(Math.max(17,49), 125));


        // TASK 2

        System.out.println("min is: " + Math.min(6,9));
        System.out.println("min is: " + Math.min(17,49));
        System.out.println("min is: " + Math.min(34.2, 12.5));
        System.out.println("min is: " + Math.min(-14, -32));
        System.out.println("min is: " + Math.min(Math.min(17,49), 125));
        System.out.println("min is: " + Math.min(Math.min(45,32) ,Math.min(56, 89)));
    }
}
