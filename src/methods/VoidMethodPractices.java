package methods;

import utilities.ScannerHelper;

public class VoidMethodPractices {
    public static void main(String[] args) {
        VoidMethodPractices.flavorPicker("chocolate");
        VoidMethodPractices.flavorPicker("vanilla");
        VoidMethodPractices.flavorPicker("strawberry");

        VoidMethodPractices.checkAge(19);
        VoidMethodPractices.checkAge(21);
        VoidMethodPractices.checkAge(55);
        VoidMethodPractices.checkAge(2);

        VoidMethodPractices.checkCreditScore(2000);
    }

    /*
    Create a public static method which will take an ice cream flavor and prints the message
    about your ice cream

    Example:
    chocolate -> You have a great taste
    vanilla -> Not bad
    strawberry -> Enjoy it!
    everything else -> give me a valid flavor
     */

    public static void flavorPicker(String flavor){
        if(flavor.equals("chocolate")) System.out.println("You have a great taste");
        else if(flavor.equals("vanilla")) System.out.println("Not bad");
        else if (flavor.equals("strawberry")) System.out.println("Enjoy it!");
        else System.out.println("Give me a valid flavor");

        // using the switch way
        switch (flavor){
            case "chocolate":
                System.out.println("You have a great taste");
                break;
            case "vanilla":
                System.out.println("Not bad");
                break;
            case "strawberry":
                System.out.println("Enjoy it!");
                break;
            default:
                System.out.println("Give me a valid flavor!");
        }



        /*
        Create a public static method named as "checkAge", it is going to take an age then prints according to following:
        0 to 19: You are a teenager
        20 to 21: You can drive
        21+: You can drink coke

         */
    }

    public static void checkAge(int age){
        if(age>=0 && age<=19) System.out.println("You are a teenager");
        else if(age>=20 && age<=21) System.out.println("You can drive");
        else if(age>21) System.out.println("You can drink coke");
        else System.out.println("Give me a valid age!");
    }


    /*
    Create a public static method named "checkCreditScore", it is going to take a balance as a parameter then it will print according to following:
    balance:
    0.00 - 999.99 -> low credit score
    1000.00 - 1999.99 -> medium credit score
    2000.00 -> 2999.99 -> good credit score
    else -> No valid credit score!

     */

    public static void checkCreditScore(double score){
        if(score>=0.00 && score<=999.99) System.out.println("low credit score");
        else if(score>=1000.00 && score<=1999.99) System.out.println("medium credit score");
        else if(score>=2000.00 && score<=2999.99) System.out.println("good credit score");
        else System.out.println("No valid credit score!");
    }
}
