package methods;

public class ReturnMethodsPractices {
    public static void main(String[] args) {
        System.out.println(ReturnMethodsPractices.findDifference(6,9));
        System.out.println(ReturnMethodsPractices.findDifference(75.43, 32.98));

        System.out.println(ReturnMethodsPractices.isIncluded("Blue", "Bluee"));

    }
    /*
    Create a public static method named as "findDifference" it will take two values and returns the difference

    Example:
    6, 9 -> 3
    4, 1 -> 3
    99, 23 -> 76
     */

    public static int findDifference(int num1, int num2){
        return Math.abs(num1-num2);
    }
    public static double findDifference(double num1, double num2){
        return Math.abs(num1-num2);
    }

    /*
    Create a public static method named "isIncluded" which takes 2 String values
    and returns true if the small string is inside the other one

    Example:
    "John", "John Doe" ->
     */

    public static boolean isIncluded(String str1, String str2){
        if(str1.length()>str2.length()){
            if(str1.contains(str2)) return true;
        }
        else if(str2.length()>str1.length()){
            if(str2.contains(str1)) return true;
        }
        return false;
    }
}
