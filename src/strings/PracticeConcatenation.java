package strings;

public class PracticeConcatenation {
    public static void main(String[] args) {
        String wordPart1 = "Le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";

        String word = wordPart1.concat(wordPart2).concat(wordPart3);
        System.out.println(word);

        String sentencePart1 = "I can";
        String sentencePart2 = "learn Java";

        String word1 = sentencePart1.concat(" ").concat(sentencePart2);
        System.out.println(word1);
    }
}
