package strings;

public class StringConcatenation {
    public static void main(String[] args) {

        //Declaration
        String city;

        //Assignment
        city = "Berlin"; //empty string

        //Re-assignment
        city = "Brugge";

        System.out.println(city);



        String s1 = "Hello";
        String s2 = "World";

        String s3 = s1 + " " + s2;  //"Hello" + "World"
        System.out.println(s3);  // Hello World

        String firstName = "Hamza";
        String lastName = "Batroukh";

        String fullname = firstName + " " + lastName;
        System.out.println(fullname);

        String fullname2 = firstName.concat(" ").concat(lastName);
        System.out.println(fullname2);


    }
}
