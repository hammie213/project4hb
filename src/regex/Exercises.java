package regex;

import java.util.Arrays;
import java.util.regex.Pattern;

public class Exercises {
    public static void main(String[] args) {
        System.out.println(Pattern.matches("[\\d]{3}-[\\d]{2}-[\\d]{4}", "----00-0000"));

        System.out.println("Task2");
        System.out.println(Pattern.matches("\\([\\d]{3}\\)-[\\d]{3}-[\\d]{4}", "630-123-4567"));
        System.out.println(Pattern.matches("[()0-9-]{14}", ""));

        System.out.println("Task3");
        String str = "How much wood would a wood chuck chuck if a wood chuck could chuck wood";
        System.out.println(str.replaceAll("wood", "****"));

        System.out.println("Task4");
        String str2 = "abc 123 $#^";
        System.out.println(str.replaceAll("[ $#^]", ""));
        System.out.println(str.replaceAll("[\\W]", ""));
        System.out.println(str.replaceAll("[\\W_]", ""));

        System.out.println("\n----------EXERCISE 1---------\n");
        System.out.println(Pattern.matches("[A-Za-z]{2,} [A-Za-z]{2,}", "John Doe"));
        System.out.println(Pattern.matches("[A-Za-z ]{2,}", "John Doe"));

        System.out.println("\n----------EXERCISE 2-----------\n");
        str = "##A1%%b2#$%C^&3";

        System.out.println(Arrays.toString(str.replaceAll("[A-Za-z]", "").toCharArray()));
        System.out.println(Arrays.toString(str.replaceAll("[\\D]", "").split("")));
    }
}
