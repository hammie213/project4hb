package regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Exercise_01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a username");
        String username = input.next();

        Pattern pattern = Pattern.compile("[a-zA-Z0-9]{5,10}");

        if(username.matches("[a-zA-Z0-9]{5,10}")) System.out.println("Valid UserName");
        else System.out.println("ERROR! Username must be 5 to 10 characters long and can only contain letters and numbers");
    }
}
