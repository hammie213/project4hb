package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherClass {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("SDET");
        Matcher matcher = pattern.matcher("TechGlobal provides students SDET test");

        System.out.println(matcher.matches());

        while(matcher.find()){
            System.out.println(matcher.group());
            System.out.println(matcher.start());
            System.out.println(matcher.end());
        }

        System.out.println(Pattern.matches("[_a-zA-Z-]{9,15}", "Tech_Global"));




    }
}
