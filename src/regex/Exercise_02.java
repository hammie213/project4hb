package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise_02 {
    public static void main(String[] args) {
        String sentence = ScannerHelper.getAString();

        Pattern pattern = Pattern.compile("[A-Za-z]{1,}");
        Matcher matcher = pattern.matcher(sentence);
        int wordCount = 0;

        while(matcher.find()){
            System.out.println(matcher.group());
            wordCount++;
        }
        System.out.println("This sentence contains " + wordCount + " words");
    }
}
