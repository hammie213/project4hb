package regex;

import java.util.Arrays;
import java.util.regex.Pattern;

public class PatternClass {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("[A-Za-z0-9 ]{5,10}");

        System.out.println(pattern);
        System.out.println(pattern.pattern());
        System.out.println(pattern.toString());

        System.out.println(Pattern.matches(pattern.pattern(), "Apple 1")); //is based on the regex conditions above

        String str = "I go to school at TechGlobal and i love it";

        String[] arr = str.split("[a-z0-9]{5,10}");
        System.out.println(Arrays.toString(arr));
    }
}
