package loops.do_while_loops;

import java.util.Scanner;

public class Exercise01_Get10FromUser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number;

        do{
            System.out.println("Please enter a number");
            number = scanner.nextInt();
        }
        while(number < 10);
        System.out.println("This number is more than or equal to 10");

        System.out.println("WHILE LOOPS SOLUTION");

        System.out.println("Please enter a number");
        int num1 = scanner.nextInt();

        while(num1 < 10){
            System.out.println("This number is not more than or equal to 10");
            System.out.println("Please enter a number");
            num1 = scanner.nextInt();
        }

        System.out.println("This number is more than or equal to 10");

    }
}
