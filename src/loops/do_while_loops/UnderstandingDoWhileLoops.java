package loops.do_while_loops;

public class UnderstandingDoWhileLoops {
    public static void main(String[] args) {
        int i = 0;

        do{
            System.out.println(i);
            i++;
        }
        while(i < 5);
    }
}
