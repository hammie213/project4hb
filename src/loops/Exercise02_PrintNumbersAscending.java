package loops;

public class Exercise02_PrintNumbersAscending {
    public static void main(String[] args) {
        //1st way beginner way
        for(int i = 1; i <= 10; i++){
            if(i != 10)System.out.print(i + " - ");
            else System.out.println(i);
        }

        //2nd way proper way
        String result = "";

        for(int i = 1; i <= 10; i++){
            result += i + " - ";
        }
        System.out.println(result.substring(0,result.length()-3));
    }
}
