package loops.fori_loops;

public class UnderstandingForILoop {
    public static void main(String[] args) {
        /*
        for(initialization; termnation condition; change){
        code block to be executed
        }
         */

        for(int i = 0; i <= 4; i++){
            System.out.println("Hello World!");
        }
        System.out.println("End of program");
    }
}
