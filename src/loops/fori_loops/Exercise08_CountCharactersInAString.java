package loops.fori_loops;

public class Exercise08_CountCharactersInAString {
    public static void main(String[] args) {
        String str = "TechGlobal School";

        int total = 0;
        for(int i = 0; i <= str.length()-1; i++){
            if(str.charAt(i) =='o') total++;
        }
        System.out.println(total);
    }
}
