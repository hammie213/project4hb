package loops.fori_loops;

public class Exercise01_PrintNumbersAscending {
    public static void main(String[] args) {
        int num = 0;
        for(int i = 0; i<=9; i++){
            System.out.println(++num);
        }
    }
}
