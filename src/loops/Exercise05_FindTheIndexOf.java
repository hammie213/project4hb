package loops;

public class Exercise05_FindTheIndexOf {
    public static void main(String[] args){
        /*
        Create a public static method named as findFirstIndexOf() and it will take a String,
        and a char then it will return the index of the first occurrence of the char.
        If the char doesn't exist in the String then return -1

        Example:

        "I love Java", 'a' -> 8
        "Banana", 'a' -> 1
        "Banana", 't' -> 1
         */

        System.out.println("1. " + findFirstIndexOf("I love Java", 'a'));
        System.out.println("2. " + findFirstIndexOf("Banana", 'a'));
        System.out.println("3. " + findFirstIndexOf("Banana", 'a'));
        System.out.println("4. " + findLastIndexOf("Banana", 'a'));
        System.out.println("5. " + clearChar("Banana", 't'));
        System.out.println("5. " + clearChar("Banana", 'a'));
        System.out.println("6. " + clearWord("Farty Pants", "Farty Shorts"));

    }
    public static int findFirstIndexOf(String s, char c){
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == c) return i;
        }
        return -1;
    }


    /*
Create a public static method named as findLastIndexOf() and it will take a String,
and a char then it will return the index of the last occurrence of the char. If the
char doesn't exist in the String then return -1.

Example:

"I love Java", 'a' ->  10
"Banana", 'a' -> 5
"Banana", 't' -> -1
 */
    public static int findLastIndexOf(String s, char c){
        for(int i = s.length()-1; i >= 0; i--){
            if(s.charAt(i) == c) return i;
        }
        return -1;
    }


    /*
Create a public static method named as clearChar() and it will take a String,
and a char then it will return the String without the given char.

Example:

"I love Java", 'a' ->  "I love Jv"
"Banana", 'a' -> "Bnn"
"Banana", 't' -> "Banana"
 */

    public static String clearChar(String s, char c){
        String newS = "";
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) != c){
                newS += s.charAt(i);
            }
        }
        return newS;
    }

     /*
Create a public static method named as clearWord() and it will take two Strings,
then it will return the String without the given second String. If the second
String is bigger than the first one return empty.

Example:

"I love Java", "av" ->  "I love Ja"
"Banana", 'an' -> "Ba"
"Banana", 'bananananaa' -> ""
 */

    public static String clearWord(String s1, String s2){
        String newS = "";
        if(s2.length() > s1.length()) return "";
        for(int i = 0; i < s1.length(); i++){
            if(!s1.substring(i).startsWith(s2)){
                newS += s1.charAt(i);
            }
            else{
                i += s2.length();
            }
        }
        return newS;
    }

    /*
    Create a public static method named as frontAndBack() and it will take a String,
    then it will return a String which has the most common words from start and end.

    Examples:

    "abcxxxabc" -> "abc"
    "abxxxab" -> "ab"
    "axxxa" -> "a"
    "wordxxxword" -> "word"
     */

    public static String frontAndBack(String s, String word){
        /*
        1. Create container
        2. Create a fori loop (0, s.length / 2)
         */
        String newS = "";
        for(int i = 0; i < s.length()/2; i++){
            if(s.endsWith(s.substring(0, i + 1))){
                newS = s.substring(0, i + 1);
            }
        }
        return newS;
    }

    /*
    Create a public static method named as mostRepeatedChar() and it will take a String,
    then it will return the most repeated char from String. If there is same amount of repetition
    return the first one. Hint: use a String container, and use nested loop

    Example:

    "I love Java" -> "v"
    "Banana" -> "a"
    "Python" -> "P"
     */

   
}
