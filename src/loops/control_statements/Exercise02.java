package loops.control_statements;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number");
        int num1 = input.nextInt();
        System.out.println("Please enter another number");
        int num2 = input.nextInt();

        for(int i = Math.min(num1,num2); i <= Math.max(num1,num2); i++){
            if(i == 5) continue;
            System.out.println(i);
        }

    }
}
