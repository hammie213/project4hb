package loops;

import java.util.Scanner;
//this is wrong for i
public class Exercise01_SumOfNumbersByUser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("FOR I SOLUTION");
        System.out.println("Please enter how many numbers you want to enter");
        int count1 = scanner.nextInt(); // How many numbers are we getting 3

        int sum1 = 0;

        for(int i = 1; i <= 3; i++){
            System.out.println("Please enter number " + i);
            sum1 += scanner.nextInt();
        }
        System.out.println(sum1);

        System.out.println("WHILE SOLUTION");
        int count2 = scanner.nextInt();

        int sum2 = 0;

        int start = 1;

        while(start <= count2){
            System.out.println("Please enter a number " + start);
            sum2 += scanner.nextInt();
            start++;
        }
        System.out.println(sum2);
    }
}
