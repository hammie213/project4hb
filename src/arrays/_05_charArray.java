package arrays;

import java.util.Arrays;

public class _05_charArray {
    public static void main(String[] args) {
        /*
TASK-1
Create a char array and store values below
#
$
5
A
b
H

Print the array

EXPECTED:
[#, $, 5, A, b, H]
 */

        System.out.println("\n-------TASK-1--------\n");
        char[] task1 = {'#', '$', '5', 'A', 'b', 'H'};
        System.out.println(Arrays.toString(task1));

        System.out.println("----------TASK TWO----------");
        /*
        Task 2
        Print the size of the array with a message

        Expected: The size of the array is = 6
         */

        System.out.println("The size of the array is = " + task1.length);

        System.out.println("----------TASK THREE----------");
        /*
        TASK 3
        Print each element using fori loop
        EXPECTED:
#
$
5
A
b
H
         */
        for(int i = 0; i < task1.length; i++){
            System.out.println(task1[i]);
        }

        /*
        Task 4 print each element using for each loop
         */
        System.out.println("----------TASK FOUR----------");

        for(char element : task1){
            System.out.println(element);
        }

        System.out.println("----------TASK FIVE----------");
        for(char element : task1){
            if(element >= 65 && element <= 90 || element >= 97 && element <= 122) System.out.println(element);
        }
        for(char element : task1){
           if(Character.isLetter(element)) System.out.println(element);
            }
        System.out.println("\n----------TASK-5 - fori loop-----------\n");
        for (int i = 0; i < task1.length; i++) {
            if(Character.isLetter(task1[i])) System.out.println(task1[i]);
        }


        System.out.println("----------TASK 6---------- FOR EACH LOOP");
        int uppercase = 0;
        /*
        TASK 6
        Count how many uppercase character you have in the array

        Expected:
        2
         */
        System.out.println("\n----------TASK-6 - for each loop-----------\n");

        int countU1 = 0;

        for (char character : task1) {
            if(Character.isUpperCase(character)) countU1++;
        }

        System.out.println(countU1);


        System.out.println("\n----------TASK-6 - fori loop-----------\n");

        int countU2 = 0;

        for (int i = 0; i < task1.length; i++) {
            if(Character.isUpperCase(task1[i])) countU2++;
        }

        System.out.println(countU2);


    }

}
