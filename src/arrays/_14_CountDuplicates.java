package arrays;

public class _14_CountDuplicates {
    public static void main(String[] args) {
        int[] array = {3, 5, 1, 3, 3, 19, 22, 10, 3};
        System.out.println("The number of duplicates is = " + findHowManyDuplicates(array));
        System.out.println();
    }

    public static int findHowManyDuplicates(int[] numbers){
        int duplicates = 0;
        int firstNum = numbers[0];

        for(int number : numbers){
            if(number == firstNum) duplicates++;
            else continue;
        }
        return duplicates;
    }
}
