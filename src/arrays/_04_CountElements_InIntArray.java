package arrays;

public class _04_CountElements_InIntArray{
    public static void main(String[] args) {

        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};
        /*
        negatives: 2
        positives: 5
        neutral or zeros: 3

        even: 6
        odd: 4

        max: 10
        min: -7

        sum of numbers: 28
        average of the numbers: 2
        how many unique numbers you have: 7
        how many of these numbers are represented in Fibonacci sequences: 6
        prime numbers: 2
        how many numbers can be divided by 5: 6
        absolute difference between max and min of these numbers: 17
        closest number to 9 (take the smallest if you have 2) : 8
         */

        int negatives = 0;
        int positives = 0;

        for(int number: numbers){
            if(number < 0 ) negatives++;
            else if(number > 0) positives++;
        }
        System.out.println("There are " + negatives + " negative numbers in this array");
        System.out.println("There are " + positives + " positive numbers in this array");

        /*
        Count how many even numbers you have

        Expected: 6
         */
        int evenNumbers = 0;

        for(int i = 0; i < numbers.length; i++){
            if(numbers[i] % 2 == 0) evenNumbers++;
        }
        System.out.println(evenNumbers);

        System.out.println("For each way");
        int evenNumbers2 = 0;

        for(int number : numbers){
            if(number % 2 == 0) evenNumbers2++;
        }
        System.out.println(evenNumbers2);
    }
}
