package arrays;

import java.util.Arrays;

public class _06_doubleArray_Recap {
    public static void main(String[] args) {
        double[] decimals = {1.5, 2.3, -1.3, -3.7};
        char[] chars ={'a', 'b', 'c', 'd'};

        System.out.println(Arrays.toString(decimals));
        System.out.println("The length of the array is " + decimals.length);

        Arrays.sort(decimals);

        System.out.println(Arrays.toString(decimals));

        System.out.println("using for i loop");
        for(int i = 0; i < decimals.length; i++){
            System.out.println(decimals[i] + " - " + chars[i]);
        }
    }
}
