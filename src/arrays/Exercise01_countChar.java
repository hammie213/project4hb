package arrays;

import java.util.Arrays;

public class Exercise01_countChar {
    public static void main(String[] args) {
        /*
Assume you are given as below

String word = "Java";

Count how many a letters you have in the String

EXPECTED:
2
 */

        int countA = 0;
        String word = "Java";

        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == 'a') countA++;
        }
        System.out.println(countA);

        // another way
        String word2 = "Java";  // [J, a, v, a]

        char[] wordArray = word2.toCharArray();

        System.out.println(Arrays.toString(wordArray));  // [J, a, v, a]

        // count how many A


        for(char element : wordArray){
            if(element == 'a') countA++;
        }
        System.out.println(countA);
    }
}
