package arrays;

import java.util.Arrays;

public class _11_charArray_Recap {
    public static void main(String[] args) {

        char[] name = {'H', 'a', 'm', 'z', 'a'};

        char[] name2 = new char[5];
        name2[0] = 'H';
        name2[1] = 'a';
        name2[2] = 'm';
        name2[3] = 'z';
        name2[4] = 'a';
        System.out.println(Arrays.toString(name));
        System.out.println(Arrays.toString(name2));

        for(char in : name){
            System.out.println(in);
        }
        for(int i = 0; i < name.length; i++){
            System.out.println(name[i]);
        }
    }
}
