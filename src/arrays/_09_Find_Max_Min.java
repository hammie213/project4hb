package arrays;

import java.util.Arrays;

public class _09_Find_Max_Min {
    public static void main(String[] args) {
        int[] numbers = {3, 5, 1, 67, 32};
        findMaxAndMinWithSort(numbers);
        printMaxAndMinWithoutSort(numbers);
    }

    public static void findMaxAndMinWithSort(int[] array){
        Arrays.sort(array);
        System.out.println("The max is = " + array[array.length-1]);
        System.out.println("The min is = " + array[array[0]]);
    }

    public static void printMaxAndMinWithoutSort(int[] numbers){
        int max = Integer.MIN_VALUE; //-1231232342
        int min = Integer.MAX_VALUE;

        for(int n : numbers){
            if(max > n){
                max = n;
            }
            if(min > n){
                min = n;
            }
        }
        

    }
}
