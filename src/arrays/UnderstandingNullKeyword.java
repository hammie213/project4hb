package arrays;

public class UnderstandingNullKeyword {
    public static void main(String[] args) {
        String s = null;

         // This is impossible will give error
        // System.out.println(s.toLowerCase());

        /*

        This will give an error

        String[] objects = {"remote", "null", "", null};
        for (String object : objects) {
            System.out.println(object.toUpperCase());
        }

         */
    }
}
