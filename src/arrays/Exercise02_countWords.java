package arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Exercise02_countWords {
    public static void main(String[] args) {
        /*
        Assume that you are given below String

        String sentence = "I love arrays";

        Count how many words you have in this String

        EXPECTED:
        3
         */
        //FOR I LOOP METHOD

        String sentence = "I love arrays";

        int countS = 0;

        for (int i = 0; i < sentence.length(); i++) {
            if(sentence.charAt(i) == ' ') countS++;
        }

        System.out.println(countS + 1);

        System.out.println("For each method");
        int countS2 = 0;

        for(char c: sentence.toCharArray()){
            if(c == ' ') countS2++;
        }
        System.out.println(countS2 + 1);

        System.out.println("Split method");

        System.out.println(sentence.split(" ").length);  // 3




    }
}
