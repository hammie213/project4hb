package arrays;

import java.util.Arrays;

public class _08_Find_First_Even_Odd {
    public static void main(String[] args) {
        int[] arrays = {0, 5, 3, 2, 4, 7, 10};
        String[] arrayy =  {"ABCD", "abcd", "123", "!@#"};
        printFirstEvenOdd(arrays);
        printFirstStringWithA(arrayy);
    }

    public static void printFirstEvenOdd(int[] numbers){
        int even = 0, odd = 0;
        boolean isEvenFound = false, isOddFound = false;

        for(int n : numbers){
            if(!isEvenFound && n % 2 == 0){ // if first even not found and the number is even
                even = n;
                isEvenFound = true; // marking flag as true so next time you know even has been found
            }
            else if(!isOddFound && n % 2 == 1){   // if first off not found and the number is odd
                odd = n;
                isOddFound = true;  // marking flag as true so next time you will know odd has been found
            }
            System.out.println(n);
        }
        System.out.println("First even = " + even);
        System.out.println("First odd = " + odd);
    }

    public static void printFirstStringWithA(String[] str){
        /*for(String element : str){
            int i = 0;
            if(element.startsWith("a")){
                System.out.println("First string starting with \"a\" = " + element);
                break;
            }
            else System.out.println("No String starting with \"a\"");

         */

        boolean isStrWithAFound = false;
        for(String element : str){
            if(element.toLowerCase().startsWith("a") && !isStrWithAFound){
                System.out.println("First string starting with \"a\" = " + element);
                isStrWithAFound = true;
            }

        }
        if(!isStrWithAFound){
            System.out.println("No String starting with \"a\"");
        }
    }
}


