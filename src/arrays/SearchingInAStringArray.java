package arrays;

import java.sql.SQLOutput;
import java.util.Arrays;

public class SearchingInAStringArray {
    public static void main(String[] args) {
        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

        /*
        Task-1
        Check the collection above and print true if it contains Mouse
        print false otherwise
         */

        boolean hasMouse = false;

        for(String object : objects){
            if(object.equals("Mouse")){
                hasMouse = true;
                break;
            }
        }
        System.out.println(hasMouse);

        System.out.println("----------With Binary Search----------");
        Arrays.sort(objects);

        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0);
        System.out.println(Arrays.binarySearch(objects, "Computer") >= 0);
        System.out.println(Arrays.binarySearch(objects, "iPad") >= 0);
    }
}
