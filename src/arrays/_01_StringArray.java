package arrays;

import java.sql.SQLOutput;
import java.util.Arrays;

public class _01_StringArray {
    public static void main(String[] args) {

        // 1. Declare a String array called as countries and assign size of 3
        String [] countries = new String[3];

        // 2. Assigning values to specific indexes
        countries[1] = "Spain";

        // 3. Print a specific index from an array
        System.out.println(countries[0]); // null
        System.out.println(countries[1]); // Spain

        // 4. Printing an array
        System.out.println(Arrays.toString(countries));

        /*
        Assign belgium to index of 2 and print the array once again
         */
        countries[2] = "Belgium";
        System.out.println(Arrays.toString(countries));

        // 5. Update existing element
        countries[1] = "France";
        System.out.println(Arrays.toString(countries));

        // 6. Getting the length of an array - how many elements - 3
        System.out.println(countries.length);

        // 7. Printing each element separately
        /*
        null
        France
        Belgium
         */

        System.out.println(countries[0]);
        System.out.println(countries[1]);
        System.out.println(countries[2]);

        // or another way
        for(int i = 0; i <= 2; i++){
            System.out.println("Country at index of " + i + " is = " + countries[i]);
        }
    }
}
