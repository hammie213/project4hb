package arrays;

import java.util.Arrays;

public class TwoDimensionalArrays {
    public static void main(String[] args) {
        String[][] students = {
                {"Ali", "Mehmet", "Alex"},
                {"Alex", "Regina"},
                {"Abdallah", "Newer"}
        };

        System.out.println(students[1][1]);

        System.out.println(students.length);

        System.out.println(Arrays.toString(students[0]));
        System.out.println(Arrays.toString(students[1]));
        System.out.println(Arrays.toString(students[2]));

        for (int i = 0; i < students.length; i++) {
            System.out.println(Arrays.toString(students[i]));
        }
        for(String[] groups : students){
            System.out.println(Arrays.toString(groups));
        }

        System.out.println(Arrays.deepToString(students));

        //printing each student with for each loop

        for(String[] group : students){
            for(String student : group){
                System.out.println(student);
            }
        }

        //printing each student with for i loop
        for(int i = 0; i < students.length; i++){
            for(int j = 0; j < students[i].length; j++){
                System.out.println(students[i][j]);
            }
        }
    }
}
