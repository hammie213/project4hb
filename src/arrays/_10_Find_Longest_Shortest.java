package arrays;

public class _10_Find_Longest_Shortest {
    public static void main(String[] args) {
        String[] colors = {"red", "blue", "yellow", "white"};
        printLongestShortest(colors);
    }
    public static void printLongestShortest(String[] strings){
        String shortStr = strings[0], longStr = strings[0];

        for(int i = 1; i < strings.length; i++){
            if(longStr.length() < strings[i].length()){
                longStr = strings[i];
            }
            if(shortStr.length() > strings[i].length()){
                shortStr = strings[i];
            }
        }
        System.out.println("Longest = " + longStr);
        System.out.println("Shortest = " + shortStr);
    }
}
