package arrays;

public class _13_SecondGreatest_SecondSmallest {
    public static void main(String[] args) {

        int[] numbas = {3, 5, 10, 22, 3, 1};
        printMaxMinSecondMaxAndMin(numbas);
    }

    public static int findMax(int[] numbers){
        int max = Integer.MIN_VALUE;

        for(int n : numbers){
            if(n > max) max = n;
        }
        return max;
    }

    public static int findMin(int[] numbers){
        int min = Integer.MAX_VALUE;

        for(int n : numbers){
            if(n < min) min = n;
        }
        return min;
    }

    public static void printMaxMinSecondMaxAndMin(int[] numbers){
        int secondMax = Integer.MIN_VALUE, secondMin = Integer.MAX_VALUE;

        for(int n : numbers){
            if(n > secondMax && n != findMax(numbers)){
                secondMax = n;
            }
            if(n < secondMin && n > findMin(numbers)){
                secondMin = n;
            }
        }
        System.out.println("max = " + findMax(numbers));
        System.out.println("second max = 3");
        System.out.println("min = " + findMin(numbers));
        System.out.println("second min = " + secondMin);
    }
}
