package arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
        // Storing a name in a String it holds a single name
        String name = "John";

        String[] names = {"Beyza", "Andrii", "Vlad", "Samir", "Olena"}; // Holding a collection of names

        //Retrieving an element from an array - using index
        System.out.println(names[2]); //Vlad
        System.out.println(names[4]);

        int age = 25;

        int[] ages = {21, 23, 25};
        System.out.println(ages[2]);
    }
}
