package arrays;

public class _12_PartialArray_Practices {
    public static void main(String[] args) {
        int[] numbers = {10, -3, -7, 0, 0, 7, 22};
        System.out.println(sumOfFirst3(numbers));
        System.out.println(sumOfLast5(numbers));

    }
    public static int sumOfFirst3(int[] array){
        int sum = 0;
        for(int i = 0; i < 3; i++){
            sum += array[i];
        }
        return sum;
    }

    public static int sumOfLast5(int[] array){
        int sum = 0;
        for(int i = array.length-1; i > array.length-6; i--){
            sum += array[i];
        }
        return sum;
    }
}
