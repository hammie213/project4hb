package homeworks;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework14 {
    public static void main(String[] args) {
        System.out.println("--------TASK ONE--------");
        fizzBuzz1(18);

        System.out.println("--------TASK TWO--------");
        System.out.println(fizzBuzz2(15));

        System.out.println("--------TASK THREE--------");
        System.out.println(findSumNumbers("ab110c045d"));

        System.out.println("--------TASK FOUR--------");
        System.out.println(findBiggestNumber("ab110c045d"));

    }

    public static void fizzBuzz1(int num){
        for(int i = 1; i <= num; i++){
            if(i % 3 == 0){
                if(i % 5 == 0){
                    System.out.println("FizzBuzz");
                    continue;
                }
                System.out.println("Fizz");
            }
            else if(i % 5 == 0){
                System.out.println("Buzz");
            }
            else System.out.println(i);
        }
    }

    public static String fizzBuzz2(int num){
        if(num % 3 == 0 && num % 5 == 0){
            return "FizzBuzz";
        }
        else if(num % 3 == 0) return "Fizz";
        else if(num % 5 == 0) return "Buzz";
        return num + "";
    }

    public static int findSumNumbers(String str){
        int sum = 0;

        str = str.replaceAll("[\\D]+", " "); //replaces all non numbers with a " " space
        String[] nums = str.split(" "); // split the string based on the space to get whole numbers


            for (String num : nums) {
                try {
                    sum += Integer.parseInt(num);
                }
                catch (Exception e){
            }
        }
        return sum;
    }

    public static int findBiggestNumber(String str){
        int biggestNumber = 0;

        str = str.replaceAll("[\\D]+", " "); //replaces all non numbers with space
        String[] nums = str.split(" ");

        for(String num : nums){
            try {
                if (Integer.parseInt(num) > biggestNumber) {
                    biggestNumber = Integer.parseInt(num);
                }
            }catch (Exception e){

            }
        }
        return biggestNumber;
    }

    public static String countSequenceOfCharacters(String str){
        int count = 1;

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == str.charAt(i + 1)) count++;
            else {
                result.append(String.valueOf(count)).append(str.charAt(i));
                count = 1;
            }
            if (i == str.length() - 2) result.append(String.valueOf(count)).append(str.charAt(i + 1));
        }
        return result.toString();
    }


}
