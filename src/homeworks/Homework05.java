package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {
        System.out.println("----------TASK ONE----------");

        String result = "";

        for(int i = 1; i <= 100; i++){
            if(i % 7 == 0) result += i + " - ";
        }
        System.out.println(result.substring(0,result.length()-3));

        System.out.println();
        System.out.println("----------TASK TWO----------");

        String result1 = "";

        for(int i = 1; i <= 50; i++){
            if(i % 2 == 0 && i % 3 == 0) result1 += i + " - ";
        }
        System.out.print(result1.substring(0, result1.length()-3));

        System.out.println();
        System.out.println("----------TASK THREE----------");

        String result2 = "";

        for(int i = 100; i >= 50; i--){
            if(i % 5 == 0) result2 += i + " - ";
        }
        System.out.println(result2.substring(0, result2.length()-3));

        System.out.println();
        System.out.println("----------TASK FOUR----------");

        for(int i = 0; i <= 7; i++){
            System.out.println("The square of " + i + " is = " + i*i);
        }

        System.out.println();
        System.out.println("----------TASK FIVE----------");

        int sum = 0;
        for(int i = 1; i <= 10; i++){
            sum += i;
        }
        System.out.println(sum);

        System.out.println();
        System.out.println("----------TASK SIX----------");

        int numT6 = ScannerHelper.getANumber();

        int factorial = 1;
        int i6 = 1;
        while(i6 <= numT6){
            factorial *= i6;
            i6++;
        }
        System.out.print(factorial);

        System.out.println();
        System.out.println("----------TASK SEVEN----------");

        String nameT7 = ScannerHelper.getAName();
        String nameLCT7 = nameT7.toLowerCase();
        int counterT7 = 0;

        for(int i = 0; i <= nameLCT7.length()-1; i++){
            if(nameLCT7.charAt(i)=='a' || nameLCT7.charAt(i)=='e' || nameLCT7.charAt(i)=='i' || nameLCT7.charAt(i)=='o' || nameLCT7.charAt(i)=='u') counterT7++;
        }
        System.out.println("There are " + counterT7 + " vowel letters in this full name");

        System.out.println();
        System.out.println("----------TASK EIGHT----------");

        Scanner input = new Scanner(System.in);
        int numT8;
        int sumT8 = 0;

        do{
            System.out.println("Please enter a number");
            numT8 = input.nextInt();
            sumT8 += numT8;
            if(numT8 >= 100) System.out.println("This number is already more than 100");
            else if(sumT8 >= 100) System.out.println("Sum of the entered numbers is at least 100");
        }while(sumT8 < 100);

        System.out.println();
        System.out.println("----------TASK NINE----------");

        int first = 0;
        int second = 1;
        String resultT9 = "";

        for(int i = 0; i < 7; i++){
            resultT9 += first + " - ";
            int sumT9 = first + second;
            first = second;
            second = sumT9;
        }
        System.out.println(resultT9.substring(0, resultT9.length()-3));


        System.out.println();
        System.out.println("----------TASK TEN----------");

        String nameT10 = ScannerHelper.getAName().toLowerCase();

        while(nameT10.charAt(0)!='j'){
            nameT10 = ScannerHelper.getAName().toLowerCase();
        }
        System.out.println("End of program");

    }
}
