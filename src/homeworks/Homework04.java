package homeworks;

import utilities.ScannerHelper;

public class Homework04 {
    public static void main(String[] args) {
        System.out.println();
        System.out.println("TASK 1");
        System.out.println();

        String name = ScannerHelper.getAName();
        System.out.println("The length of the name is = " + name.length());
        System.out.println("The first character in the name is = " + name.substring(0,1));
        System.out.println("The last character in the name is = " + name.substring(name.length() -1 ));
        System.out.println("The first 3 characters in the name are = " + name.substring(0,3));
        System.out.println("The last 3 characters in the name are = " + name.substring(name.length()-3));
        if(name.startsWith("A") || name.startsWith("a")) System.out.println("You are in the club!");
        else System.out.println("Sorry, you are not in the club");

        System.out.println();
        System.out.println("TASK 2");
        System.out.println();

        String address = ScannerHelper.getAnAddress();
        if(address.contains("Chicago") || address.contains("chicago")) System.out.println("You are in the club");
        else if(address.contains("Des Plaines")) System.out.println("You are welcome to join the club");
        else System.out.println("Sorry, you will never be in the club");

        System.out.println();
        System.out.println("TASK 3");
        System.out.println();

        String color = ScannerHelper.favColors();
        String color2 = color.toLowerCase();
        if(color2.contains("red")){
            if(color2.contains("green")) System.out.println("Green and red are in the list");
            else System.out.println("Red is in the list");
        }
        else if(color2.contains("green")) System.out.println("Green is in the list");
        else System.out.println("Green and red are not in the list");

        System.out.println();
        System.out.println("TASK 4");
        System.out.println();

        String str = "  Java is FUN  ";
        String strTrimmed = str.trim();
        String str1 = strTrimmed.substring(0,4);
        String str2 = strTrimmed.substring(5,7);
        String str3 = strTrimmed.substring(8,11);

        System.out.println("The first word in the str is = " + str1.toLowerCase());
        System.out.println("The second word in the str is = " + str2.toLowerCase());
        System.out.println("The third word in the str is = " + str3.toLowerCase());
    }
}
