package homeworks;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args){

        System.out.println("----------TASK ONE----------");
        int[] task1 = new int[10];

        task1[2] = 23;
        task1[4] = 12;
        task1[7] = 34;
        task1[9] = 7;
        task1[6] = 15;
        task1[0] = 89;

        System.out.println(task1[3]);
        System.out.println(task1[0]);
        System.out.println(task1[9]);

        System.out.println(Arrays.toString(task1));

        System.out.println("----------TASK TWO----------");

        String[] task2 = new String[5];

        task2[1] = "abc";
        task2[4] = "xyz";

        System.out.println(task2[3]);
        System.out.println(task2[0]);
        System.out.println(task2[4]);
        System.out.println(Arrays.toString(task2));

        System.out.println("----------TASK THREE----------");
        int[] task3 = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(task3));

        Arrays.sort(task3);
        System.out.println(Arrays.toString(task3));

        System.out.println("----------TASK FOUR----------");
        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));

        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        System.out.println("----------TASK FIVE----------");

        String[] cartoons = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoons));
        int hasPluto = 0;
        // 1st way
        for(String number : cartoons){
            if(number.equals("Pluto")){
                hasPluto++;
            }
        }
        if(hasPluto > 0) System.out.println("true");
        else System.out.println("false");

        //2nd way
        Arrays.sort(cartoons);
        if(Arrays.binarySearch(cartoons, "Pluto") > 0) System.out.println("true");
        else System.out.println("false");


        System.out.println("----------TASK SIX----------");
        String[] cartoonCats = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(cartoonCats);
        System.out.println(Arrays.toString(cartoonCats));

        //1st way of checking
        for(String cats : cartoonCats){
            if(cats.equals("Garfield") && cats.equals("Felix")){
                System.out.println(true);
                break;
            }
            else System.out.println(false);
            break;
        }

        System.out.println("----------TASK SEVEN----------");

        double[] task7 = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(task7));
        System.out.println(task7[0]);
        System.out.println(task7[1]);
        System.out.println(task7[2]);
        System.out.println(task7[3]);
        System.out.println(task7[4]);

        System.out.println("----------TASK EIGHT----------");

        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        int uppercaseCounter = 0;
        int lowercaseCounter = 0;
        int digitCounter = 0;
        int specialChar = 0;

        System.out.println(Arrays.toString(characters));
        System.out.println("Letters = " + characters.length);

        for(char chars8 : characters){
            if(chars8 >= 65 && chars8 <= 90) uppercaseCounter++;
            else if(chars8 >= 97 && chars8 <= 122) lowercaseCounter++;
        }
        System.out.println("Uppercase letters = " + uppercaseCounter);
        System.out.println("Lowercase letters = " + lowercaseCounter);

        for(int charsT8 : characters){
            if(charsT8 >= 48 && charsT8 <= 57) digitCounter++;
        }
        System.out.println("Digits = " + digitCounter);

        for(char charsTask8 : characters){
            if((charsTask8 >= 33 && charsTask8 <= 47) || (charsTask8 >= 58 && charsTask8 <= 64)) specialChar++;
        }
        System.out.println("Special characters = " + specialChar);

        System.out.println("----------TASK NINE----------");

        String[] school = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        int uppercaseT9 = 0;
        int lowercaseT9 = 0;
        int startWithBorP = 0;
        int bookOrPen = 0;

        System.out.println(Arrays.toString(school));
        for(String element : school){
            int i = 0;
            if(Character.isUpperCase(element.charAt(i))) uppercaseT9++;
            else if(Character.isLowerCase(element.charAt(i))) lowercaseT9++;
            if(element.charAt(i) == 'B' || element.charAt(i) == 'b' || element.charAt(i) == 'P' || element.charAt(i) == 'p') startWithBorP++;
            if(element.toLowerCase().contains("book") || element.toLowerCase().contains("pen")) bookOrPen++;
            i++;
        }
        System.out.println("Elements starts with uppercase = " + uppercaseT9);
        System.out.println("Elements starts with lowercase = " + lowercaseT9);
        System.out.println("Elements starting with B or P = " + startWithBorP);
        System.out.println("Elements having \"book\" or \"pen\" = " + bookOrPen);

        System.out.println("----------TASK TEN----------");

        int[] task10 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        int greaterThan10 = 0;
        int lessThan10 = 0;
        int is10 = 0;


        System.out.println(Arrays.toString(task10));
        for(int numbas : task10){
            if(numbas > 10) greaterThan10++;
            else if (numbas == 10) is10++;
            else lessThan10++;
        }
        System.out.println("Elements that are more than 10 = " + greaterThan10);
        System.out.println("Elements that are less than 10 = " + lessThan10);
        System.out.println("Elements that are 10 = " + is10);
    }
}
