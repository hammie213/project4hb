package homeworks;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("\n------TASK ONE------\n");
        String str1 = "JaVA";
        System.out.println(hasLowerCase(str1));

        System.out.println("\n------TASK TWO------\n");
        ArrayList<Integer> arrayList2 = new ArrayList<>(Arrays.asList(1, 1, 0));
        System.out.println(noZero(arrayList2));

        System.out.println("\n------TASK THREE------\n");
        int[] numbers = {1,2,3};
        System.out.println(numberAndSquare(numbers));

        System.out.println("\n------TASK FOUR------\n");
        String[] array4 = {"abc", "foo", "java"};
        String str4 = "foo";
        System.out.println(containsValue(array4, str4));

        System.out.println("\n------TASK FIVE------\n");
        String str5 = "Java is fun";
        System.out.println(reverseSentence(str5));
    }

    public static boolean hasLowerCase(String str){
        Pattern pattern = Pattern.compile("[a-z]");
        Matcher matcher = pattern.matcher(str);

        return matcher.find();
    }

    public static ArrayList<Integer> noZero(ArrayList<Integer> arrayList){
        for(int i = 0; i < arrayList.size(); i++){
            if(arrayList.get(i) == 0){
                arrayList.remove(i);
            }
        }
        return arrayList;
    }
    public static int[][] numberAndSquare(int[] arr){
        int[][] result = new int[arr.length][2];
        for (int i = 0; i < arr.length; i++) {
            result[i][0] = arr[i];
            result[i][1] = arr[i] * arr[i];
        }
        return result;
    }

    public static boolean containsValue(String[] arr, String str){
        Pattern pattern = Pattern.compile(str);
        Matcher matcher = pattern.matcher(Arrays.toString(arr));

        return matcher.find();
    }

    public static String reverseSentence(String str){
        String s[] = str.split(" ");
        String result = "";

        for(int i = 0; i < s.length; i++){
            result = s[i] + " " + result;
        }
        return result;
    }

}
