package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework10 {
    public static void main(String[] args) {
        System.out.println("------TASK ONE------");
        String str = "Selenium is the most common UI automation tool.   ";
        System.out.println(countWords(str));

        System.out.println("\n------TASK TWO------\n");
        String str2 = "QA stands for Quality Assurance";
        System.out.println(countA(str2));

        System.out.println("\n------TASK THREE------\n");
        int[] arr3 = {-23, -4, 0, 2, 5, 90, 123};
        System.out.println(countPos(arr3));

        System.out.println("\n------TASK FOUR------\n");
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3));
        System.out.println(removeDuplicateNumbers(numbers));

        System.out.println("\n------TASK FIVE------\n");
        ArrayList<String> strings = new ArrayList<>(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"));
        System.out.println(removeDuplicateElements(strings));

        System.out.println("\n------TASK SIX------\n");
        String str6 = "Java    is fun   ";
        System.out.println(removeExtraSpaces(str6));

        System.out.println("\n------TASK SEVEN------\n");
        int[] arr1 = {3, 0, 0, 7, 5, 10};
        int[] arr2 = {6, 3, 2};
        System.out.println(add(arr1, arr2));
    }

    public static int countWords(String str){
        if(str.replaceAll("[^a-zA-Z]", "").isEmpty()) return 0;
        return str.trim().split("[\\s+]").length;
        //first you trim to remove all extra spaces before and after text
        //split length is basically counting how many white spaces there are (\\s)
    }

    public static int countA(String str){
        return str.split("[aA]").length-1;
    }

    public static int countPos(int[] array){
        return (int) Arrays.stream(array).filter(x -> x > 0).count();
    }

    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> numbers){
        ArrayList<Integer> number = new ArrayList<>();

        for(Integer num : numbers){
            if(number.contains(num)) continue;
            else number.add(num);
        }
        return number;
    }

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> str){
        ArrayList<String> strings = new ArrayList<>();

        for(String string : str){
            if(strings.contains(string)) continue;
            else strings.add(string);
        }
        return strings;
    }

    public static String removeExtraSpaces(String str){
        return str.trim().replaceAll("\\s+", " ");
    }

    public static int[] add(int[] arr1, int[] arr2) {
        int larger = Math.max(arr1.length, arr2.length);
        int smaller = Math.min(arr1.length, arr2.length);
        int[] array = new int[larger];

        for(int i = 0; i < smaller; i++){
            array[i] = arr1[i] + arr2[i];
        }

       for(int i = smaller; i < larger; i++){
           if(arr1.length > arr2.length) array[i] = arr1[i];
           else array[i] = arr2[i];
       }
       return array;
    }


//    public static int findClosestTo10(int[] arr) {
//        Arrays.sort(arr);
//
//        int before10=0;
//        int after10=0;
//
//        for (int j : arr) {
//            if (j < 10) before10 = j;
//        }
//
//        for(int i = arr.length - 1; i--){
//            if(arr[i] > 10) before10 = arr[i];
//        }
//
//        if(arr[0] >= 10) return after10;
//        else if(arr[arr.length-1] <= 10) return before10;
//        else {
//            if(Math.abs(10-before10) <= Math.abs(10-after10)) return before10;
//            else return after10;
//        }
//    }

}
