package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework11 {
    public static void main(String[] args) {
        System.out.println("\n------TASK ONE------\n");
        String str1 = "      Hello     ";
        System.out.println(noSpace(str1));

        System.out.println("\n------TASK TWO------\n");
        String str2 = "     A    ";
        System.out.println(replaceFirstLast(str2));

        System.out.println("\n------TASK THREE------\n");
        String str3 = "ABC";
        System.out.println(hasVowel(str3));

        System.out.println("\n------TASK FOUR------\n");
        int yearOfBirth = 2003;
        checkAge(yearOfBirth);

        System.out.println("\n------TASK FIVE------\n");
        int num1 = 10, num2 = 13, num3 = 20;
        System.out.println(averageOfEdges(num1, num2, num3));

        System.out.println("\n------TASK SIX------\n");
        String[] array = {"appium", "123", "ABC", "java"};
        System.out.println(Arrays.toString(noA(array)));

        System.out.println("\n------TASK SEVEN------\n");
        int[] array2 = {3, 4, 5, 6};
        System.out.println(Arrays.toString(no3or5(array2)));

        System.out.println("\n------TASK EIGHT------\n");
        int[] numbers = {41, 53, 19, 47, 67};
        System.out.println(countPrimes(numbers));


    }
    public static String noSpace(String str){
        str = str.trim();
        return str;
    }
    public static String replaceFirstLast(String str){
        str = str.trim();
        if(str.length() < 2) return "";
        return str.charAt(str.length()-1)+ str.substring(1,str.length()-1) + str.charAt(0);
    }

    public static boolean hasVowel(String str){
        str = str.toLowerCase();
        if(str.contains("a") || str.contains("e") || str.contains("i") || str.contains("o") || str.contains("u")) return true;
        return false;
    }

    public static void checkAge(int yearOfBirth){
        // 2006 is 16
        // 1922 is 100
        if(yearOfBirth > 2006 && yearOfBirth <= 2022) System.out.println("AGE IS NOT ALLOWED");
        else if(yearOfBirth <= 2006 && yearOfBirth >= 1922) System.out.println("AGE IS ALLOWED");
        else System.out.println("AGE IS NOT VALID");
    }

    public static int averageOfEdges(int num1, int num2, int num3){
        int min = Math.min(Math.min(num1,num2), num3);
        int max = Math.max(Math.max(num1, num2), num3);

        return (min + max)/2;
    }

    public static String[] noA(String[] array){
        ArrayList<String> withoutA = new ArrayList<>();

        for(String str : array){
            if(str.toLowerCase().charAt(0) == 'a') withoutA.add("###");
            else withoutA.add(str);
        }
        return withoutA.toArray(new String[0]);
    }

    public static int[] no3or5(int[] numbers){
        ArrayList<Integer> number = new ArrayList<>();

        for(int num : numbers){
            if(num % 5 == 0){
                if(num % 3 == 0){
                    number.add(101);
                }
                number.add(99);
            }
            else if(num % 3 == 0){
                number.add(100);
            }
            else number.add(num);
        }
        int[] nums = number.stream().mapToInt(i -> i).toArray();
        // This method I found converts from ArrayList into an array effortlessly without loops

        return nums;
    }

    public static int countPrimes(int[] array){
        int counter = 0;
        for(int num : array){
            if(num > 2 && num % 1 == 0 && num % num == 0 && num % 2 != 0){
                counter++;
            }
        }
        return counter;
    }
}
