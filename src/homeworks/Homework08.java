package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("------Task 1------");
        String str = "hello";
        System.out.println(countConsonants(str));
        System.out.println("\n------Task 2------\n");
        String str2 = "Hello, nice to meet you!!";
        System.out.println(wordArray(str2));
        System.out.println("\n------Task 3------\n");
        String str3 = "Hello,    nice to   meet      you!!";
        System.out.println(removeExtraSpaces(str3));
        System.out.println("\n------Task 4------\n");
        Scanner scanner = new Scanner(System.in);
        String str4 = scanner.nextLine();
        System.out.println(count3OrLess(str4));
        System.out.println("\n------Task 5-----\n");
        String dateOfBirth = "01/21/1999";
        System.out.println(isDateFormatValid(dateOfBirth));
        System.out.println("\n------Task 6------\n");
        String email = "abc@student.techglobal.com";
        System.out.println(isEmailFormatValid(email));

    }
    public static int countConsonants(String string){
        int counter = 0;
        Pattern pattern = Pattern.compile("[a-zA-Z&&[^aeiouAEIOU]]");
        Matcher matcher = pattern.matcher(string);

        while(matcher.find()){
            counter++;
        }
        return counter;
    }

    public static List<String> wordArray(String string){
         String str2 = string.replace(' ', ',');
         return Arrays.asList(str2);
    }

    public static String removeExtraSpaces(String string){
        return string.replaceAll("[\\s]+", " ");
    }

    public static int count3OrLess(String input){
        int counter = 0;

        String[] array = input.split(" ");
        for(String str : array){
            if(str.matches("[a-zA-Z]{0,3}")) counter++;
        }
        return counter;
    }


    public static boolean isDateFormatValid(String dateOfBirth){
        return Pattern.matches("[\\d]{2}/[\\d]{2}/[\\d]{4}", dateOfBirth);
    }

    public static boolean isEmailFormatValid(String email){
        return Pattern.matches("/[\\w.#$-]{2,}@[\\w.]{2,}\\.[\\w]{2,}/gm", email);
    }
}
