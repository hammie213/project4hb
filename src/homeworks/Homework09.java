package homeworks;

import java.util.ArrayList;

public class Homework09 {
    public static void main(String[] args) {
        System.out.println("------TASK ONE------");
        int[] numbers = {-4, 0, -7, 0, 5, 10, 45, 45};
        System.out.println(firstDuplicatedNumber(numbers));

        System.out.println("------TASK TWO------");
        String[] words = {"a", "b", "B", "XYZ", "123"};
        System.out.println(firstDuplicatedString(words));

        System.out.println("------TASK THREE------");
        int[] numbers1 = {0, -4, -7, 0, 5, 10, 45, -7, 0};
        System.out.println(allDuplicatedInts(numbers1));

        System.out.println("------TASK FOUR------");
        String[] task4 = {"A", "foo", "12", "Foo", "bar", "a", "a", "java"};
        System.out.println(allDuplicatedStrings(task4));

        System.out.println("------TASK FIVE------");
        String[] task5 = {"abc", "foo", "bar"};
        System.out.println(reverseElements(task5));

        System.out.println("------TASK SIX------");
        String task6 = "Java is fun";
        System.out.println(reverseWords(task6));
    }

    public static int firstDuplicatedNumber(int[] nums){
        int number = 0;
        boolean foundDuplicated = false;
        for(int i = 0; i < nums.length; i++){
            for(int j = i+1; j < nums.length; j++){
                if(nums[i] == nums[j] && foundDuplicated == false) {
                    foundDuplicated = true;
                    number = nums[i];
                }
            }
        }
        return number;
    }

    public static String firstDuplicatedString(String[] words){
        String result = "";
        boolean foundDuplicate = false;

        for(int i = 0; i < words.length; i++){
            for(int j = i + 1; j < words.length; j++){
                if(words[i].toLowerCase().equals(words[j].toLowerCase()) && foundDuplicate == false){
                    foundDuplicate = true;
                    result = words[i];
                    return result;
                }
            }
        }
        return "There is no duplicates";
    }

    public static String allDuplicatedInts(int[] numbers){
        ArrayList<Integer> alreadyHave = new ArrayList<>();
        ArrayList<Integer> numbers1 = new ArrayList<>();
        for(int i = 0; i < numbers.length; i++){
            for(int j = i + 1; j < numbers.length; j++){
                if(numbers[i] == numbers[j] && !alreadyHave.contains(numbers[i])){
                    alreadyHave.add(numbers[i]);
                    numbers1.add(numbers[i]);
                }
            }
        }
        return numbers1.toString();
    }

    public static String allDuplicatedStrings(String[] array){
        ArrayList<String> allDuplicates = new ArrayList<>();
        for(int i = 0; i < array.length-1; i++){
            for(int j = i+1; j < array.length; j++){
                if(array[i].equalsIgnoreCase(array[j]) && !allDuplicates.contains(array[i].toLowerCase())){
                    allDuplicates.add(array[i].toLowerCase());
                }
            }
        }
        if(allDuplicates.size() == 0) return "There is no duplicates";
        return allDuplicates.toString();
    }
    public static ArrayList<String> reverseElements(String[] words){
        ArrayList<String> result = new ArrayList<>();
        for(int i = words.length-1; i >= 0; i--){
            result.add(words[i]);
        }
        return result;
    }

    public static String reverseWords(String str){
        String reverse = "";

        String[] array = str.split("[ ]+");

        for(String s : array){
            reverse += new StringBuilder(s).reverse() + " ";
        }
        return reverse;
    }
}



