package homeworks;

import java.util.ArrayList;

import java.util.Collections;

import java.util.Arrays;

public class Homework07 {
    public static void main(String[] args) {
        System.out.println("----------TASK ONE----------");
        ArrayList<Integer> numbersT1 = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));
        System.out.println(numbersT1.get(3));
        System.out.println(numbersT1.get(0));
        System.out.println(numbersT1.get(2));
        System.out.println(numbersT1);

        System.out.println("\n----------TASK TWO----------\n");
        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));
        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n----------TASK THREE----------\n");
        ArrayList<Integer> numbersT3 = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));
        System.out.println(numbersT3);
        Collections.sort(numbersT3);
        System.out.println(numbersT3);

        System.out.println("\n----------TASK FOUR----------\n");
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n----------TASK FIVE----------\n");
        ArrayList<String> marvel = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panther", "Deadpool", "Captain America"));
        System.out.println(marvel);
        if(marvel.contains("Wolwerine")){
            System.out.println(true);
        }
        else System.out.println(false);

        System.out.println("\n----------TASK SIX----------\n");
        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));
        Collections.sort(avengers);
        System.out.println(avengers);
        if(avengers.contains("Hulk") && avengers.contains("Iron Man")) System.out.println(true);
        else System.out.println(false);

        System.out.println("\n----------TASK SEVEN----------\n");
        ArrayList<Character> charsT7 = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));
        System.out.println(charsT7);
        System.out.println(charsT7.get(0));
        System.out.println(charsT7.get(1));
        System.out.println(charsT7.get(2));
        System.out.println(charsT7.get(3));
        System.out.println(charsT7.get(4));
        System.out.println(charsT7.get(5));
        System.out.println(charsT7.get(6));
        System.out.println(charsT7.get(7));
        System.out.println(charsT7.get(8));

        System.out.println("\n----------TASK EIGHT----------\n");
        int counter = 0;
        int notCounter = 0;
        ArrayList<String> computerStuff = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));
        System.out.println(computerStuff);
        Collections.sort(computerStuff);
        System.out.println(computerStuff);
        for(String element : computerStuff){
            if(element.toLowerCase().startsWith("m")) counter++;
            if(!element.toLowerCase().contains("a") && !element.toLowerCase().contains("e")) notCounter++;
        }
        System.out.println(counter);
        System.out.println(notCounter);

        System.out.println("");

        System.out.println("\n----------TASK 9----------");
        int counterT9 = 0;
        int lowerCounterT9 = 0;
        int pCounter = 0;
        int pStartOrEnd = 0;
        ArrayList<String> kitchenObjects = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));
        System.out.println(kitchenObjects);
        for(String element : kitchenObjects){
            if(element.charAt(0) >= 65 && element.charAt(0) <= 90){
                counterT9++;
            }
            else if(element.charAt(0) >= 97 && element.charAt(0) <= 122){
                lowerCounterT9++;
            }
            if(element.toLowerCase().contains("p")){
                pCounter++;
            }
            if(element.toLowerCase().charAt(0) == 'p' || element.toLowerCase().charAt(element.length()-1) == 'p'){
                pStartOrEnd++;
            }
        }
        System.out.println("Elements start with uppercase = " + counterT9);
        System.out.println("Elements starts with lowercase = " + lowerCounterT9);
        System.out.println("Elements having P or p = " + pCounter);
        System.out.println("Elements starting or ending with P or p = " + pStartOrEnd);

        System.out.println("\n----------TASK TEN----------\n");
        int divided10 = 0;
        int evenGreater15 = 0;
        int oddLessThan20 = 0;
        int lessThan15OrGreaterThan50 = 0;
        ArrayList<Integer> task10 = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));
        System.out.println(task10);

        for(int number : task10){
            if(number % 10 == 0){
                divided10++;
            }
            if(number % 2 == 0 && number > 15){
                evenGreater15++;
            }
            if(number % 2 != 0 && number < 20){
                oddLessThan20++;
            }
            if(number < 15 || number > 50){
                lessThan15OrGreaterThan50++;
            }

        }
        System.out.println("Elements that can be divided by 10 = " + divided10);
        System.out.println("Elements that are even and greater than 15 = " + evenGreater15);
        System.out.println("Elements that are odd and less than 20 = " + oddLessThan20);
        System.out.println("Elements that are less than 15 or greater than 50 = " + lessThan15OrGreaterThan50);
    }
}
