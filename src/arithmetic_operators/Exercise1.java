package arithmetic_operators;

public class Exercise1 {
    public static void main(String[] args) {
        /*
        Assume you have a rectangle with a short side of 4 units and a long side equal to 7 units, find the area and perimeter of rectangle

        Area = shortSide * longSide
        Perimeter = 2 * shortSide + 2 * longSide
        Perimeter = 2 (shortSide + longSide)

        Expected output:
        Area is = 28
        Perimeter = 22
         */

        int shortSide = 4;
        int longSide = 7;
        int area = shortSide * longSide;

        int perimeter = (2 * shortSide) + (2 * longSide);
        System.out.println("Area is = " + area);
        System.out.println("Perimeter = " + perimeter);

        System.out.println(-10+7*5);
    }
}
