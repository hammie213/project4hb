package arithmetic_operators;

public class UnderstandingArithmeticOperators {
    public static void main(String[] args) {
        System.out.println(3 + 5);
        System.out.println(5 - 2);
        System.out.println(10 / 2);
        System.out.println(2 * 4);
        System.out.println(10 % 3);

        System.out.println();
        // create 2 int variables and store 6 and 4 in these variables

        int num1 = 6;
        int num2 = 4;

        int sum = num1 + num2;
        System.out.println(sum);

        int subtraction = num1 - num2;
        System.out.println(subtraction);

        int product = num1 * num2;
        System.out.println(product);

        int division = num1 / num2;
        System.out.println(division);

        int remainder = num1 % num2;
        System.out.println(remainder);

        System.out.println("The sum of the numbers is = " + (num1 + num2));
        System.out.println("");
    }
}
