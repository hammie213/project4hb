package arithmetic_operators;

import java.util.Scanner;

public class Exercise3 {
    public static void main(String[] args) {
        /*
        Task: Calculate monthly payment
        Ask user to enter yearly salary
        Then calculate and tell them back
        how much they make monthly

        Program: Hey user, how much do you make yearly?
        User: 120000

        Program: Your monthly payment is $10000
         */
        Scanner input = new Scanner(System.in);
        double yearly, monthly;


        System.out.println("Hey user, how much you make yearly?");
        yearly = input.nextDouble();

        monthly = yearly / 12;

        System.out.println("Your monthly payment is $" + monthly);

    }
}
