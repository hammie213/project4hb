package arithmetic_operators;

public class Exercise2 {
    public static void main(String[] args) {
        /*
        An annual average salary for an SDET in the USA is 90k. Write a Java program that calculates and prints monthly and bi-weekly and weekly average amount that an SDET makes in the USA.

        Expected Output
        Monthly: 7500
        Bi-weekly: 3461
        Weekly: 1730
         */

        int yearlySalary = 90000;

        int monthly = yearlySalary / 12;
        int biWeekly = yearlySalary / 26;
        int weekly = yearlySalary / 52;

        System.out.println("Monthly: " + monthly);
        System.out.println("Bi-weekly: " + biWeekly);
        System.out.println("Weekly: " + weekly);
    }
}
